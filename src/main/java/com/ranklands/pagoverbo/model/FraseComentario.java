package com.ranklands.pagoverbo.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "frase_comentario")
public class FraseComentario implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "titulo", nullable = false)
	private String titulo;

	@Column(name = "texto", nullable = false)
	private String textoComentario;

	@Column(name = "data_comentario", nullable = false)
	private LocalDate dataComentario;

	@Column(name = "autor_comentario", nullable = false)
	private String autorComentario;

	@JsonBackReference
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "frase_id", nullable = false)
	private Frase frase;

	public FraseComentario() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FraseComentario(Integer id, String titulo, String textoComentario, LocalDate dataComentario,
			String autorComentario, Frase frase) {
		super();
		this.id = id;
		this.titulo = titulo;
		this.textoComentario = textoComentario;
		this.dataComentario = dataComentario;
		this.autorComentario = autorComentario;
		this.frase = frase;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getTextoComentario() {
		return textoComentario;
	}

	public void setTextoComentario(String textoComentario) {
		this.textoComentario = textoComentario;
	}

	public LocalDate getDataComentario() {
		return dataComentario;
	}

	public void setDataComentario(LocalDate dataComentario) {
		this.dataComentario = dataComentario;
	}

	public String getAutorComentario() {
		return autorComentario;
	}

	public void setAutorComentario(String autorComentario) {
		this.autorComentario = autorComentario;
	}

	public Frase getFrase() {
		return frase;
	}

	public void setFrase(Frase frase) {
		this.frase = frase;
	}

}
