package com.ranklands.pagoverbo.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.ranklands.pagoverbo.model.enums.TipoConta;

@Entity
@Table(name = "conta_autor")
public class ContaAutor implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "nome_completo_titular", nullable = false)
	private String nomeCompletoTitular;

	@Column(name = "banco", nullable = false)
	private String banco;

	@Column(name = "numero_banco", nullable = false)
	private String numeroBanco;

	@Column(name = "agencia", nullable = false)
	private String agencia;

	@Column(name = "conta", nullable = false)
	private String conta;

	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_conta", nullable = false)
	private TipoConta tipoConta;

	@JsonIgnore
	@Column(name = "operacao", nullable = true)
	private String operacao;

	@Column(name = "cpf_cnpj", nullable = false)
	private String cpfCnpj;

	@Column(name = "digito_agencia", nullable = true)
	private String digitoAgencia;

	@Column(name = "digito_conta", nullable = false)
	private String digitoConta;

	@Column(name = "digito_banco", nullable = false)
	private String digitoBanco;

	@JsonBackReference
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinColumn(name = "autor_id", nullable = false)
	private Autor autor;

	public ContaAutor() {
		super();
	}

	public ContaAutor(Integer id, String nomeCompletoTitular, String banco, String agencia,
			String conta, TipoConta tipoConta, String cpfCnpj,
			String digitoConta, Autor autor) {
		super();
		this.id = id;
		this.nomeCompletoTitular = nomeCompletoTitular;
		this.banco = banco;
		this.agencia = agencia;
		this.conta = conta;
		this.tipoConta = tipoConta;
		this.cpfCnpj = cpfCnpj;
		this.digitoConta = digitoConta;
		this.autor = autor;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNomeCompletoTitular() {
		return nomeCompletoTitular;
	}

	public void setNomeCompletoTitular(String nomeCompletoTitular) {
		this.nomeCompletoTitular = nomeCompletoTitular;
	}

	public String getNumeroBanco() {
		return numeroBanco;
	}

	public void setNumeroBanco(String numeroBanco) {
		this.numeroBanco = numeroBanco;
	}

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public String getConta() {
		return conta;
	}

	public void setConta(String conta) {
		this.conta = conta;
	}

	public TipoConta getTipoConta() {
		return tipoConta;
	}

	public void setTipoConta(TipoConta tipoConta) {
		this.tipoConta = tipoConta;
	}

	public String getOperacao() {
		return operacao;
	}

	public void setOperacao(String operacao) {
		this.operacao = operacao;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public Autor getAutor() {
		return autor;
	}

	public void setAutor(Autor autor) {
		this.autor = autor;
	}

	public String getDigitoAgencia() {
		return digitoAgencia;
	}

	public void setDigitoAgencia(String digitoAgencia) {
		this.digitoAgencia = digitoAgencia;
	}

	public String getDigitoConta() {
		return digitoConta;
	}

	public void setDigitoConta(String digitoConta) {
		this.digitoConta = digitoConta;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getDigitoBanco() {
		return digitoBanco;
	}

	public void setDigitoBanco(String digitoBanco) {
		this.digitoBanco = digitoBanco;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ContaAutor other = (ContaAutor) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
