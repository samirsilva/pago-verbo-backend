package com.ranklands.pagoverbo.model.enums;

import com.fasterxml.jackson.annotation.JsonValue;


public enum TipoConta {

	CORRENTE("Corrente"),
	POUPANÇA("Poupança");

	private final String valor;

	TipoConta(String valor) {
		this.valor = valor;
	}

	@JsonValue
	public String getValor() {
		return this.valor;
	}

}
