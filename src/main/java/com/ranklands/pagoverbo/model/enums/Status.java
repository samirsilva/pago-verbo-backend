package com.ranklands.pagoverbo.model.enums;

import com.fasterxml.jackson.annotation.JsonValue;


public enum Status {

	PENDENTE("Pendente"),
	RECUSADO("Recusado"),
	TRANSFERIDO("Transferido");

	private final String valor;

	Status(String valor) {
		this.valor = valor;
	}

	@JsonValue
	public String getValor() {
		return this.valor;
	}

}
