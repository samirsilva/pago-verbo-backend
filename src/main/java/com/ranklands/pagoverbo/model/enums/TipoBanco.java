package com.ranklands.pagoverbo.model.enums;

import com.fasterxml.jackson.annotation.JsonValue;

public enum TipoBanco {

	BANCO_DO_BRASIL("001"),
	SANTANDER("033"),
	CAIXA_ECONÔMICA("104"),
	BRADESCO("237"),
	ITAU("341"),
	BANRISUL("041"),
	SICREDI("748"),
	SICOOB("756"),
	INTER("077"),
	BRB("070");

	private final String valor;

	TipoBanco(String valor) {
		this.valor = valor;
	}

	@JsonValue
	public String getValor() {
		return this.valor;
	}

}
