package com.ranklands.pagoverbo.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.ranklands.pagoverbo.model.enums.TipoUsuario;

@Entity
@Table(name = "autor")
public class Autor implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "nome", nullable = false)
	private String nome;

	@Column(name = "sobrenome", nullable = false)
	private String sobrenome;

	@Column(name = "idade", nullable = true)
	private Integer idade;

	@Column(unique = true)
	private String email;

	@Column(name = "cpf", unique = true, nullable = false)
	private String cpf;

	@Column(name = "senha", nullable = false)
	private String senha;

	@Column(name = "plano", nullable = false)
	private Boolean plano;

	@Column(name = "saldo", nullable = false)
	private BigDecimal saldo;

	@Column(name = "contador_frase", nullable = false)
	private Integer contadorFrases;

	@Column(name = "contador_texto", nullable = false)
	private Integer contadorTexto;

	@Column(name = "biografia", nullable = true)
	private String biografia;

	@Column(name = "exp", nullable = false)
	private Integer exp;

	@Column(name = "nivel", nullable = false)
	private Integer nivel;

	@Column(name = "perfil_completo", nullable = false)
	private Integer perfilCompleto;

	@Column(name = "dia_plano", nullable = false)
	private Integer diaPlano;

	@Column(name = "idade_ok", nullable = true)
	private Boolean idadeOk;

	@Column(name = "biografia_ok", nullable = true)
	private Boolean biografiaOk;

	@Column(name = "voto_positivo", nullable = false)
	private Integer votoPositivo;

	@Column(name = "voto_negativo", nullable = false)
	private Integer votoNegativo;

	@Column(name = "mes_referencia_voto", nullable = false)
	private Integer mesReferenciaVoto;

	@Column(name = "data_monitoramento_plano", nullable = false)
	private LocalDateTime dataMonitoramentoPlano;

	@Column(name = "data_cadastro", nullable = false)
	private LocalDate dataCadastro;
	
	@Column(name = "posicao", nullable = false)
	private Integer posicao;

	@ElementCollection(fetch = FetchType.EAGER)
	@CollectionTable(name = "tipo_autor")
	@Column(name = "perfil_id")
	private Set<Integer> tiposDeUsuario = new HashSet<Integer>();

	@JsonManagedReference
	@OneToOne(fetch = FetchType.EAGER, optional = true, cascade = { CascadeType.ALL })
	@JoinColumn(name = "conta_autor_id")
	private ContaAutor contaAutor = new ContaAutor();

	@JsonManagedReference
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "autor", cascade = { CascadeType.ALL })
	private Set<Texto> textos = new HashSet<Texto>();

	@JsonManagedReference
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "autor", cascade = { CascadeType.ALL })
	private Set<Frase> frases = new HashSet<Frase>();

	@JsonManagedReference
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "autor", cascade = { CascadeType.REMOVE, CascadeType.MERGE})
	private Set<NotificacaoAutor> notificacoes = new HashSet<NotificacaoAutor>();

	@JsonManagedReference
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "autor", cascade = { CascadeType.ALL })
	private Set<Titulo> titulos = new HashSet<Titulo>();

	@JsonManagedReference
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "autor", cascade = { CascadeType.REMOVE, CascadeType.MERGE })
	private Set<SolicitacaoSaque> saques = new HashSet<SolicitacaoSaque>();

	@JsonManagedReference
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "autor", cascade = { CascadeType.ALL })
	private Set<VotoAutorFrase> votosAutorFrases = new HashSet<VotoAutorFrase>();

	@JsonManagedReference
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "autor", cascade = { CascadeType.ALL})
	private Set<VotoAutorTexto> votosAutorTextos = new HashSet<VotoAutorTexto>();

	public Autor() {
		addTipoUsuario(TipoUsuario.USUARIO);
	}

	public Autor(Integer id, String nome, String sobrenome, Integer idade, String email, String cpf, String senha,
			Boolean plano, BigDecimal saldo, Integer contadorFrases, Integer contadorTexto, String biografia,
			Integer exp, Integer nivel, ContaAutor contaAutor, Integer perfilCompleto, Integer diaPlano,
			Boolean idadeOk, Boolean biografiaOk, Integer votoPositivo, Integer votoNegativo, Integer mesReferenciaVoto,
			LocalDate dataCadastro, LocalDateTime dataMonitoramentoPlano, Integer posicao) {
		super();
		this.id = id;
		this.nome = nome;
		this.sobrenome = sobrenome;
		this.idade = idade;
		this.email = email;
		this.cpf = cpf;
		this.senha = senha;
		this.plano = plano;
		this.saldo = saldo;
		this.contadorFrases = contadorFrases;
		this.contadorTexto = contadorTexto;
		this.biografia = biografia;
		this.exp = exp;
		this.nivel = nivel;
		this.contaAutor = contaAutor;
		this.perfilCompleto = perfilCompleto;
		this.diaPlano = diaPlano;
		this.idadeOk = idadeOk;
		this.biografiaOk = biografiaOk;
		this.votoPositivo = votoPositivo;
		this.votoNegativo = votoNegativo;
		this.mesReferenciaVoto = mesReferenciaVoto;
		this.dataCadastro = dataCadastro;
		this.dataMonitoramentoPlano = dataMonitoramentoPlano;
		this.posicao = posicao;
		addTipoUsuario(TipoUsuario.USUARIO);

	}

	public Autor(Integer id, String nome, String sobrenome, Integer idade, String email, String biografia) {
		super();
		this.id = id;
		this.nome = nome;
		this.sobrenome = sobrenome;
		this.idade = idade;
		this.email = email;
		this.biografia = biografia;

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public Integer getIdade() {
		return idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Boolean getPlano() {
		return plano;
	}

	public void setPlano(Boolean plano) {
		this.plano = plano;
	}

	public BigDecimal getSaldo() {
		return saldo;
	}

	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}

	public Integer getContadorFrases() {
		return contadorFrases;
	}

	public void setContadorFrases(Integer contadorFrases) {
		this.contadorFrases = contadorFrases;
	}

	public Integer getContadorTexto() {
		return contadorTexto;
	}

	public void setContadorTexto(Integer contadorTexto) {
		this.contadorTexto = contadorTexto;
	}

	public String getBiografia() {
		return biografia;
	}

	public void setBiografia(String biografia) {
		this.biografia = biografia;
	}

	public Integer getExp() {
		return exp;
	}

	public void setExp(Integer exp) {
		this.exp = exp;
	}

	public Integer getNivel() {
		return nivel;
	}

	public void setNivel(Integer nivel) {
		this.nivel = nivel;
	}

	public Integer getPerfilCompleto() {
		return perfilCompleto;
	}

	public void setPerfilCompleto(Integer perfilCompleto) {
		this.perfilCompleto = perfilCompleto;
	}

	public Integer getDiaPlano() {
		return diaPlano;
	}

	public void setDiaPlano(Integer diaPlano) {
		this.diaPlano = diaPlano;
	}

	public Boolean getIdadeOk() {
		return idadeOk;
	}

	public void setIdadeOk(Boolean idadeOk) {
		this.idadeOk = idadeOk;
	}

	public Boolean getBiografiaOk() {
		return biografiaOk;
	}

	public void setBiografiaOk(Boolean biografiaOk) {
		this.biografiaOk = biografiaOk;
	}

	public Integer getVotoPositivo() {
		return votoPositivo;
	}

	public void setVotoPositivo(Integer votoPositivo) {
		this.votoPositivo = votoPositivo;
	}

	public Integer getVotoNegativo() {
		return votoNegativo;
	}

	public Integer getMesReferenciaVoto() {
		return mesReferenciaVoto;
	}

	public void setMesReferenciaVoto(Integer mesReferenciaVoto) {
		this.mesReferenciaVoto = mesReferenciaVoto;
	}

	public void setVotoNegativo(Integer votoNegativo) {
		this.votoNegativo = votoNegativo;
	}

	public LocalDateTime getDataMonitoramentoPlano() {
		return dataMonitoramentoPlano;
	}

	public void setDataMonitoramentoPlano(LocalDateTime dataMonitoramentoPlano) {
		this.dataMonitoramentoPlano = dataMonitoramentoPlano;
	}

	public LocalDate getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(LocalDate dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Set<TipoUsuario> getTiposDeUsuario() {
		return tiposDeUsuario.stream().map(x -> TipoUsuario.toEnum(x)).collect(Collectors.toSet());
	}

	public void addTipoUsuario(TipoUsuario tipo) {
		tiposDeUsuario.add(tipo.getCod());
	}

	public ContaAutor getContaAutor() {
		return contaAutor;
	}

	public void setContaAutor(ContaAutor contaAutor) {
		this.contaAutor = contaAutor;
	}

	public Set<Texto> getTextos() {
		return textos;
	}

	public void setTextos(Set<Texto> textos) {
		this.textos = textos;
	}

	public Set<Frase> getFrases() {
		return frases;
	}

	public void setFrases(Set<Frase> frases) {
		this.frases = frases;
	}

	public Set<NotificacaoAutor> getNotificacoes() {
		return notificacoes;
	}

	public void setNotificacoes(Set<NotificacaoAutor> notificacoes) {
		this.notificacoes = notificacoes;
	}

	public Set<Titulo> getTitulos() {
		return titulos;
	}

	public void setTitulos(Set<Titulo> titulos) {
		this.titulos = titulos;
	}

	public Set<SolicitacaoSaque> getSaques() {
		return saques;
	}

	public void setSaques(Set<SolicitacaoSaque> saques) {
		this.saques = saques;
	}

	public Set<VotoAutorFrase> getVotosAutorFrases() {
		return votosAutorFrases;
	}

	public void setVotosAutorFrases(Set<VotoAutorFrase> votosAutorFrases) {
		this.votosAutorFrases = votosAutorFrases;
	}

	public Set<VotoAutorTexto> getVotosAutorTextos() {
		return votosAutorTextos;
	}

	public void setVotosAutorTextos(Set<VotoAutorTexto> votosAutorTextos) {
		this.votosAutorTextos = votosAutorTextos;
	}

	public Integer getPosicao() {
		return posicao;
	}

	public void setPosicao(Integer posicao) {
		this.posicao = posicao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Autor other = (Autor) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Usuario: ");
		builder.append(nome);
		builder.append(sobrenome);
		builder.append(", Email: ");
		builder.append(email);
		builder.append(cpf);
		return builder.toString();
	}

}
