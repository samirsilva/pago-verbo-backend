package com.ranklands.pagoverbo.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "texto_comentario")
public class TextoComentario implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;
	
	@Column(name = "titulo", nullable = false)
	private String titulo;
	
	@Column(name = "texto", nullable = false)
	private String textoComentario;
	
	@Column(name = "data_comentario", nullable = false)
	private LocalDate dataComentario;
	
	@Column(name = "autor_comentario", nullable = false)
	private String autorComentario;
	
	@JsonBackReference
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "texto_id", nullable = false)
	private Texto texto;

	public TextoComentario() {
		super();
	}

	public TextoComentario(Integer id, String titulo, String textoComentario, LocalDate dataComentario,
			String autorComentario, Texto texto) {
		super();
		this.id = id;
		this.titulo = titulo;
		this.textoComentario = textoComentario;
		this.dataComentario = dataComentario;
		this.autorComentario = autorComentario;
		this.texto = texto;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getTextoComentario() {
		return textoComentario;
	}

	public void setTextoComentario(String textoComentario) {
		this.textoComentario = textoComentario;
	}

	public LocalDate getDataComentario() {
		return dataComentario;
	}

	public void setDataComentario(LocalDate dataComentario) {
		this.dataComentario = dataComentario;
	}

	public String getAutorComentario() {
		return autorComentario;
	}

	public void setAutorComentario(String autorComentario) {
		this.autorComentario = autorComentario;
	}

	public Texto getTexto() {
		return texto;
	}

	public void setTexto(Texto texto) {
		this.texto = texto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TextoComentario other = (TextoComentario) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
