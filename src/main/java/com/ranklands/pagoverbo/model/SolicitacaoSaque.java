package com.ranklands.pagoverbo.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.ranklands.pagoverbo.model.enums.Status;

@Entity
@Table(name = "solicitacao_saque")
public class SolicitacaoSaque implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Enumerated(EnumType.STRING)
	@Column(name = "status", nullable = false)
	private Status status;

	@Column(name = "valor_saque", nullable = false)
	private BigDecimal valorSaque;

	@Column(name = "descricao", nullable = false)
	private String descricao;

	@Column(name = "data_saque", nullable = false)
	private LocalDate dataSaque;

	@Column(name = "motivo_alteracao_status", nullable = true)
	private String motivoAlteracaoStatus;
	
	@Column(name = "notificacao", nullable = false)
	private Boolean notificacao;
	
	@JsonBackReference
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "autor_id", nullable = false)
	private Autor autor;

	public SolicitacaoSaque() {
		super();
	}

	public SolicitacaoSaque(Integer id, Status status, BigDecimal valorSaque, String descricao, Autor autor) {
		super();
		this.id = id;
		this.status = status;
		this.valorSaque = valorSaque;
		this.descricao = descricao;
		this.autor = autor;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public BigDecimal getValorSaque() {
		return valorSaque;
	}

	public void setValorSaque(BigDecimal valorSaque) {
		this.valorSaque = valorSaque;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Autor getAutor() {
		return autor;
	}

	public void setAutor(Autor autor) {
		this.autor = autor;
	}

	public LocalDate getDataSaque() {
		return dataSaque;
	}

	public void setDataSaque(LocalDate dataSaque) {
		this.dataSaque = dataSaque;
	}

	public String getMotivoAlteracaoStatus() {
		return motivoAlteracaoStatus;
	}

	public void setMotivoAlteracaoStatus(String motivoAlteracaoStatus) {
		this.motivoAlteracaoStatus = motivoAlteracaoStatus;
	}

	public Boolean getNotificacao() {
		return notificacao;
	}

	public void setNotificacao(Boolean notificacao) {
		this.notificacao = notificacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SolicitacaoSaque other = (SolicitacaoSaque) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
