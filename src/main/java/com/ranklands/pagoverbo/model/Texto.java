package com.ranklands.pagoverbo.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "texto")
public class Texto implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "texto", unique = true, nullable = false)
	private String texto;

	@Column(name = "data_lancamento", nullable = false)
	private LocalDate dataLancamento;

	@Column(name = "voto_positivo", nullable = true)
	private Integer votoPositivo;;

	@Column(name = "voto_negativo", nullable = true)
	private Integer votoNegativo;;

	@Column(name = "categoria", nullable = false)
	private String categoria;

	@Column(name = "mes_referencia_voto", nullable = false)
	private Integer mesReferenciaVoto;

	@JsonBackReference
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "autor_id", nullable = false)
	private Autor autor;

	@JsonManagedReference
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "texto", cascade = { CascadeType.ALL })
	private Set<TextoComentario> textoComentarios = new HashSet<TextoComentario>();

	public Texto() {
		super();
	}

	public Texto(Integer id, String texto, LocalDate dataLancamento, Integer votoPositivo, Integer votoNegativo,
			Autor autor, String categoria, Integer mesReferenciaVoto) {
		super();
		this.id = id;
		this.texto = texto;
		this.dataLancamento = dataLancamento;
		this.votoPositivo = votoPositivo;
		this.votoNegativo = votoNegativo;
		this.autor = autor;
		this.categoria = categoria;
		this.mesReferenciaVoto = mesReferenciaVoto;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public LocalDate getDataLancamento() {
		return dataLancamento;
	}

	public void setDataLancamento(LocalDate dataLancamento) {
		this.dataLancamento = dataLancamento;
	}

	public Integer getVotoPositivo() {
		return votoPositivo;
	}

	public void setVotoPositivo(Integer votoPositivo) {
		this.votoPositivo = votoPositivo;
	}

	public Integer getVotoNegativo() {
		return votoNegativo;
	}

	public void setVotoNegativo(Integer votoNegativo) {
		this.votoNegativo = votoNegativo;
	}

	public Autor getAutor() {
		return autor;
	}

	public void setAutor(Autor autor) {
		this.autor = autor;
	}

	public Set<TextoComentario> getTextoComentarios() {
		return textoComentarios;
	}

	public void setTextoComentarios(Set<TextoComentario> textoComentarios) {
		this.textoComentarios = textoComentarios;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public Integer getMesReferenciaVoto() {
		return mesReferenciaVoto;
	}

	public void setMesReferenciaVoto(Integer mesReferenciaVoto) {
		this.mesReferenciaVoto = mesReferenciaVoto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Texto other = (Texto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
