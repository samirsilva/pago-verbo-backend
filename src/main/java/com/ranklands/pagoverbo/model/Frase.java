package com.ranklands.pagoverbo.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "frase")
public class Frase implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "texto", unique = true, nullable = false)
	private String texto;

	@Column(name = "data_lancamento", nullable = false)
	private LocalDate dataLancamento;

	@Column(name = "voto_positivo", nullable = true)
	private Integer votoPositivo;;

	@Column(name = "voto_negativo", nullable = true)
	private Integer votoNegativo;;

	@Column(name = "mes_referencia_voto", nullable = false)
	private Integer mesReferenciaVoto;
	
	@Column(name = "nome_autor", nullable = false)
	private String nomeAutor;
	
	@JsonBackReference
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "autor_id", nullable = false)
	private Autor autor;

	@JsonManagedReference
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "frase", cascade = { CascadeType.ALL })
	private Set<FraseComentario> fraseComentarios = new HashSet<FraseComentario>();

	public Frase() {
		super();
	}

	public Frase(Integer id, String texto, LocalDate dataLancamento, Integer votoPositivo, Integer votoNegativo,
			Autor autor, Integer mesReferenciaVoto, String nomeAutor) {
		super();
		this.id = id;
		this.texto = texto;
		this.dataLancamento = dataLancamento;
		this.votoPositivo = votoPositivo;
		this.votoNegativo = votoNegativo;
		this.autor = autor;
		this.mesReferenciaVoto = mesReferenciaVoto;
		this.nomeAutor = nomeAutor;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public LocalDate getDataLancamento() {
		return dataLancamento;
	}

	public void setDataLancamento(LocalDate dataLancamento) {
		this.dataLancamento = dataLancamento;
	}

	public Integer getVotoPositivo() {
		return votoPositivo;
	}

	public void setVotoPositivo(Integer votoPositivo) {
		this.votoPositivo = votoPositivo;
	}

	public Integer getVotoNegativo() {
		return votoNegativo;
	}

	public void setVotoNegativo(Integer votoNegativo) {
		this.votoNegativo = votoNegativo;
	}

	public Autor getAutor() {
		return autor;
	}

	public void setAutor(Autor autor) {
		this.autor = autor;
	}

	public Set<FraseComentario> getFraseComentarios() {
		return fraseComentarios;
	}

	public void setFraseComentarios(Set<FraseComentario> fraseComentarios) {
		this.fraseComentarios = fraseComentarios;
	}

	
	public Integer getMesReferenciaVoto() {
		return mesReferenciaVoto;
	}

	public void setMesReferenciaVoto(Integer mesReferenciaVoto) {
		this.mesReferenciaVoto = mesReferenciaVoto;
	}

	
	public String getNomeAutor() {
		return nomeAutor;
	}

	public void setNomeAutor(String nomeAutor) {
		this.nomeAutor = nomeAutor;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Frase other = (Frase) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
