package com.ranklands.pagoverbo.model.parametro;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "parametro_banco")
public class ParametroBanco implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "saldo", nullable = false)
	private BigDecimal saldo;

	@Column(name = "descricao", nullable = false)
	private String descricao;

	@Column(name = "data_atualizacao", nullable = false)
	private LocalDate dataAtualizacao;
	
	@Column(name = "taxa_voto_frase", nullable = false)
	private BigDecimal taxaVotoFrase;
	
	@Column(name = "taxa_insercao_frase", nullable = false)
	private BigDecimal taxaInsercaoFrase;
	
	@Column(name = "taxa_voto_texto", nullable = false)
	private BigDecimal taxaVotoTexto;
	
	@Column(name = "taxa_insercao_texto", nullable = false)
	private BigDecimal taxaInsercaoTexto;

	public ParametroBanco() {
		super();
	}

	public ParametroBanco(Integer id, BigDecimal saldo, String descricao, LocalDate dataAtualizacao,
			BigDecimal taxaVotoFrase, BigDecimal taxaInsercaoFrase, BigDecimal taxaVotoTexto,
			BigDecimal taxaInsercaoTexto) {
		super();
		this.id = id;
		this.saldo = saldo;
		this.descricao = descricao;
		this.dataAtualizacao = dataAtualizacao;
		this.taxaVotoFrase = taxaVotoFrase;
		this.taxaInsercaoFrase = taxaInsercaoFrase;
		this.taxaVotoTexto = taxaVotoTexto;
		this.taxaInsercaoTexto = taxaInsercaoTexto;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getSaldo() {
		return saldo;
	}

	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public LocalDate getDataAtualizacao() {
		return dataAtualizacao;
	}

	public void setDataAtualizacao(LocalDate dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}

	public BigDecimal getTaxaVotoFrase() {
		return taxaVotoFrase;
	}

	public void setTaxaVotoFrase(BigDecimal taxaVotoFrase) {
		this.taxaVotoFrase = taxaVotoFrase;
	}

	public BigDecimal getTaxaInsercaoFrase() {
		return taxaInsercaoFrase;
	}

	public void setTaxaInsercaoFrase(BigDecimal taxaInsercaoFrase) {
		this.taxaInsercaoFrase = taxaInsercaoFrase;
	}

	public BigDecimal getTaxaVotoTexto() {
		return taxaVotoTexto;
	}

	public void setTaxaVotoTexto(BigDecimal taxaVotoTexto) {
		this.taxaVotoTexto = taxaVotoTexto;
	}

	public BigDecimal getTaxaInsercaoTexto() {
		return taxaInsercaoTexto;
	}

	public void setTaxaInsercaoTexto(BigDecimal taxaInsercaoTexto) {
		this.taxaInsercaoTexto = taxaInsercaoTexto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParametroBanco other = (ParametroBanco) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
