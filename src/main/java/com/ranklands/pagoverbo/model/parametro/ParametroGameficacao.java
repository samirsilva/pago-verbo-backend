package com.ranklands.pagoverbo.model.parametro;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "parametro_gameficacao")
public class ParametroGameficacao implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "multiplicador_exp_free", nullable = false)
	private Integer multiplicadorExpFree;

	@Column(name = "multiplicador_exp_pago", nullable = false)
	private Integer multiplicadorExpPago;

	@Column(name = "valor_exp_frase", nullable = false)
	private Integer valorExpFrase;

	@Column(name = "valor_exp_texto", nullable = false)
	private Integer valorExpTexto;

	@Column(name = "valor_exp_voto_frase", nullable = false)
	private Integer valorExpVotoFrase;

	@Column(name = "valor_exp_voto_texto", nullable = false)
	private Integer valorExpVotoTexto;

	@Column(name = "valor_exp_comentario", nullable = false)
	private Integer valorExpComentario;

	public ParametroGameficacao() {
		super();
	}

	public ParametroGameficacao(Integer id, Integer multiplicadorExpFree, Integer multiplicadorExpPago,
			Integer valorExpFrase, Integer valorExpTexto, Integer valorExpVotoFrase, Integer valorExpVotoTexto,
			Integer valorExpComentario) {
		super();
		this.id = id;
		this.multiplicadorExpFree = multiplicadorExpFree;
		this.multiplicadorExpPago = multiplicadorExpPago;
		this.valorExpFrase = valorExpFrase;
		this.valorExpTexto = valorExpTexto;
		this.valorExpVotoFrase = valorExpVotoFrase;
		this.valorExpVotoTexto = valorExpVotoTexto;
		this.valorExpComentario = valorExpComentario;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getMultiplicadorExpFree() {
		return multiplicadorExpFree;
	}

	public void setMultiplicadorExpFree(Integer multiplicadorExpFree) {
		this.multiplicadorExpFree = multiplicadorExpFree;
	}

	public Integer getMultiplicadorExpPago() {
		return multiplicadorExpPago;
	}

	public void setMultiplicadorExpPago(Integer multiplicadorExpPago) {
		this.multiplicadorExpPago = multiplicadorExpPago;
	}

	public Integer getValorExpFrase() {
		return valorExpFrase;
	}

	public void setValorExpFrase(Integer valorExpFrase) {
		this.valorExpFrase = valorExpFrase;
	}

	public Integer getValorExpTexto() {
		return valorExpTexto;
	}

	public void setValorExpTexto(Integer valorExpTexto) {
		this.valorExpTexto = valorExpTexto;
	}

	public Integer getValorExpVotoFrase() {
		return valorExpVotoFrase;
	}

	public void setValorExpVotoFrase(Integer valorExpVotoFrase) {
		this.valorExpVotoFrase = valorExpVotoFrase;
	}

	public Integer getValorExpVotoTexto() {
		return valorExpVotoTexto;
	}

	public void setValorExpVotoTexto(Integer valorExpVotoTexto) {
		this.valorExpVotoTexto = valorExpVotoTexto;
	}

	public Integer getValorExpComentario() {
		return valorExpComentario;
	}

	public void setValorExpComentario(Integer valorExpComentario) {
		this.valorExpComentario = valorExpComentario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParametroGameficacao other = (ParametroGameficacao) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
