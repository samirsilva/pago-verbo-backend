package com.ranklands.pagoverbo.model.parametro;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "parametro_titulo")
public class ParametroTitulo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id;

	@Column(name = "nome", nullable = false)
	private String nome;

	@Column(name = "descricao", nullable = false)
	private String descricao;

	@Column(name = "quantidade", nullable = false)
	private Integer quantidade;

	@Column(name = "exp", nullable = false)
	private Integer exp;

	@Column(name = "tipo_voto", nullable = false)
	private Boolean tipo_voto;
	
	@Column(name = "tipo_frase_texto", nullable = false)
	private Boolean tipoFraseTexto;

	public ParametroTitulo() {
		super();
	}

	public ParametroTitulo(Integer id, String nome, String descricao, Integer quantidade, Integer exp,
			Boolean tipo_voto, Boolean tipoFraseTexto) {
		super();
		this.id = id;
		this.nome = nome;
		this.descricao = descricao;
		this.quantidade = quantidade;
		this.exp = exp;
		this.tipo_voto = tipo_voto;
		this.tipoFraseTexto = tipoFraseTexto;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Integer getExp() {
		return exp;
	}

	public void setExp(Integer exp) {
		this.exp = exp;
	}

	public Boolean getTipo_voto() {
		return tipo_voto;
	}

	public void setTipo_voto(Boolean tipo_voto) {
		this.tipo_voto = tipo_voto;
	}

	public Boolean getTipoFraseTexto() {
		return tipoFraseTexto;
	}

	public void setTipoFraseTexto(Boolean tipoFraseTexto) {
		this.tipoFraseTexto = tipoFraseTexto;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParametroTitulo other = (ParametroTitulo) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
