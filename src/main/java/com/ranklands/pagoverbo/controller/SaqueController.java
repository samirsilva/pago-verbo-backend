package com.ranklands.pagoverbo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ranklands.pagoverbo.model.SolicitacaoSaque;
import com.ranklands.pagoverbo.request.AlterarSaqueRequest;
import com.ranklands.pagoverbo.request.SolicitacaoSaqueRequest;
import com.ranklands.pagoverbo.service.SaqueService;

@RestController
@RequestMapping(value = "/saques")
public class SaqueController {

	@Autowired
	private SaqueService serviceSaque;

	@PreAuthorize("hasAnyRole('USUARIO')")
	@RequestMapping(value = "/solicitar/{idAutor}", method = RequestMethod.POST)
	public ResponseEntity<SolicitacaoSaque> insert(@PathVariable Integer idAutor, @RequestBody SolicitacaoSaqueRequest request) {
		SolicitacaoSaque saque = serviceSaque.insert(request, idAutor);
		return ResponseEntity.ok().body(saque);
	}

	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(value = "/alterar/{id}", method = RequestMethod.PUT)
	public ResponseEntity<SolicitacaoSaque> insert(@PathVariable Integer id, @RequestBody AlterarSaqueRequest request) {
		SolicitacaoSaque saque = serviceSaque.update(request, id);
		return ResponseEntity.ok().body(saque);
	}

	@PreAuthorize("hasAnyRole('USUARIO')")
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<SolicitacaoSaque>> findAllByData() {
		List<SolicitacaoSaque> list = serviceSaque.findAll();
		return ResponseEntity.ok().body(list);
	}
}
