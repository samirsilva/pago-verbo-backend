package com.ranklands.pagoverbo.controller;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.ranklands.pagoverbo.model.Autor;
import com.ranklands.pagoverbo.model.ContaAutor;
import com.ranklands.pagoverbo.request.NovoUsuarioRequest;
import com.ranklands.pagoverbo.request.AtivarPlanoRequest;
import com.ranklands.pagoverbo.request.AutorUpdateRequest;
import com.ranklands.pagoverbo.request.ContaAutorRequest;
import com.ranklands.pagoverbo.service.AutorService;

import javassist.NotFoundException;

@RestController
@RequestMapping(value = "/autores")
public class AutorController {

	@Autowired
	private AutorService serviceAutor;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Autor> find(@PathVariable Integer id) {
		Autor obj = serviceAutor.find(id);

		return ResponseEntity.ok().body(obj);
	}

	@RequestMapping(value="/email", method=RequestMethod.GET)
	public ResponseEntity<Autor> find(@RequestParam(value="value") String email) {
		Autor obj = serviceAutor.findByEmail(email);
		return ResponseEntity.ok().body(obj);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> insert(@Valid @RequestBody NovoUsuarioRequest requestUser) {
		Autor autor = serviceAutor.fromDTO(requestUser);
		autor = serviceAutor.insert(autor);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(autor.getId()).toUri();

		return ResponseEntity.created(uri).build();
	}
	
	@PreAuthorize("hasAnyRole('USUARIO')")
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> update(@Valid @RequestBody AutorUpdateRequest request, @PathVariable Integer id) {
		Autor autor = serviceAutor.fromDTO(request);
		autor.setId(id);
		autor = serviceAutor.update(autor);

		return ResponseEntity.noContent().build();
	}
	
	@PreAuthorize("hasAnyRole('USUARIO')")
	@RequestMapping(value = "/conta", method = RequestMethod.POST)
	public ResponseEntity<Void> insertContaAutor(@Valid @RequestBody ContaAutorRequest request) throws NotFoundException {
		ContaAutor conta = serviceAutor.fromDTO(request);
		conta = serviceAutor.insertContaAutor(conta);

		return ResponseEntity.noContent().build();
	}

	@PreAuthorize("hasAnyRole('USUARIO')")
	@RequestMapping(value = "/conta/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> updateContaAutor(@Valid @RequestBody ContaAutorRequest request, @PathVariable Integer id) throws NotFoundException {
		ContaAutor conta = serviceAutor.fromDTO(request);
		conta.setId(id);
		conta = serviceAutor.updateContaAutor(conta);

		return ResponseEntity.noContent().build();
	}

	@PreAuthorize("hasAnyRole('USUARIO')")
	@RequestMapping(value = "/ativar/plano/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Void> ativaPlano(@Valid @RequestBody AtivarPlanoRequest request, @PathVariable Integer id) throws NotFoundException {
		this.serviceAutor.ativarPlanoPago(request, id);
		
		return ResponseEntity.noContent().build();
	}
	
	@PreAuthorize("hasAnyRole('USUARIO')")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable Integer id) {
		serviceAutor.delete(id);
		return ResponseEntity.noContent().build();
	}

	@PreAuthorize("hasAnyRole('USUARIO')")
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<AutorUpdateRequest>> findAllByVoto() {
		List<Autor> list = serviceAutor.findAll();
		List<AutorUpdateRequest> listDto = list.stream().map(obj -> new AutorUpdateRequest(obj)).collect(Collectors.toList());

		return ResponseEntity.ok().body(listDto);
	}
	
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(value = "/page" , method = RequestMethod.GET)
	public ResponseEntity<Page<AutorUpdateRequest>> findPage(
			@RequestParam(name= "page", defaultValue = "0") Integer page, 
			@RequestParam(name= "linesPage", defaultValue = "24")Integer linesPage, 
			@RequestParam(name= "orderBy", defaultValue = "nome")String orderBy, 
			@RequestParam(name= "direction", defaultValue = "DESC")String direction) {
		
		Page<Autor> list = serviceAutor.findPage(page, linesPage, orderBy, direction);
		Page<AutorUpdateRequest> listDto = list.map(obj -> new AutorUpdateRequest(obj));

		return ResponseEntity.ok().body(listDto);
	}	
	
	@RequestMapping(value="/picture", method=RequestMethod.POST)
	public ResponseEntity<Void> uploadProfilePicture(@RequestParam(name="file") MultipartFile file) {
		URI uri = serviceAutor.uploadProfilePicture(file);
		return ResponseEntity.created(uri).build();
	}
}
