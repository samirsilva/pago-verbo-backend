package com.ranklands.pagoverbo.controller;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.ranklands.pagoverbo.model.Frase;
import com.ranklands.pagoverbo.request.FraseRequest;
import com.ranklands.pagoverbo.request.VotoFraseRequest;
import com.ranklands.pagoverbo.service.FraseService;

import javassist.NotFoundException;

@RestController
@RequestMapping(value = "/frases")
public class FraseController {

	@Autowired
	private FraseService service;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<Frase> find(@PathVariable Integer id) {
		Frase obj = service.find(id);

		return ResponseEntity.ok().body(obj);
	}

	@PreAuthorize("hasAnyRole('ADMIN', 'USUARIO')")
	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<Void> insert(@Valid @RequestBody FraseRequest request) throws NotFoundException {
		Frase obj = service.fromDTO(request);
		obj = service.insert(obj);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId()).toUri();

		return ResponseEntity.created(uri).build();
	}
	
	@PreAuthorize("hasAnyRole('ADMIN', 'USUARIO')")
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Frase> update(@Valid @RequestBody FraseRequest request, @PathVariable Integer id) throws NotFoundException {
		Frase obj = service.update(request, id);
		return ResponseEntity.ok().body(obj);
	}

	@PreAuthorize("hasAnyRole('ADMIN', 'USUARIO')")
	@RequestMapping(value = "/votar/{id}", method = RequestMethod.PUT)
	public ResponseEntity<Frase> updateVoto(@Valid @RequestBody VotoFraseRequest requestVoto, @PathVariable Integer id) throws NotFoundException {
		Frase obj = service.updateVoto(requestVoto, id);
		return ResponseEntity.ok().body(obj);
	}
	
	@PreAuthorize("hasAnyRole('ADMIN', 'USUARIO')")
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable Integer id) {
		service.delete(id);
		return ResponseEntity.noContent().build();
	}
	
	@PreAuthorize("hasAnyRole('ADMIN', 'USUARIO')")
	@RequestMapping(value = "/page" , method = RequestMethod.GET)
	public ResponseEntity<Page<Frase>> findPage(
			@RequestParam(name= "page", defaultValue = "0") Integer page, 
			@RequestParam(name= "linesPage", defaultValue = "5")Integer linesPage, 
			@RequestParam(name= "orderBy", defaultValue = "votoPositivo")String orderBy, 
			@RequestParam(name= "direction", defaultValue = "DESC")String direction) {
		
		Page<Frase> list = service.findPage(page, linesPage, orderBy, direction);
		list.map(obj -> new Frase());

		return ResponseEntity.ok().body(list);
	}		
}
