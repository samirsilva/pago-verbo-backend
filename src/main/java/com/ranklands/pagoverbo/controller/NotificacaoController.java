package com.ranklands.pagoverbo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ranklands.pagoverbo.model.NotificacaoAutor;
import com.ranklands.pagoverbo.service.NotificacaoAutorService;

@RestController
@RequestMapping(value = "/notificacoes")
public class NotificacaoController {

	@Autowired
	private NotificacaoAutorService serviceNotificacao;

	@PreAuthorize("hasAnyRole('USUARIO')")
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<List<NotificacaoAutor>> findAllByVoto(@PathVariable Integer id) {
		List<NotificacaoAutor> list = serviceNotificacao.findAll(id);
		return ResponseEntity.ok().body(list);
	}

}
