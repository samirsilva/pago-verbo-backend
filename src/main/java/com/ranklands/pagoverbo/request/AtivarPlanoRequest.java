package com.ranklands.pagoverbo.request;

import java.io.Serializable;

public class AtivarPlanoRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer diasPlano;

	public AtivarPlanoRequest() {

	}

	public AtivarPlanoRequest(Integer diasPlano) {
		super();
		this.diasPlano = diasPlano;
	}

	public Integer getDiasPlano() {
		return diasPlano;
	}

	public void setDiasPlano(Integer diasPlano) {
		this.diasPlano = diasPlano;
	}

}
