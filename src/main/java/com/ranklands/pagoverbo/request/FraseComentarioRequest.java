package com.ranklands.pagoverbo.request;

import java.io.Serializable;
import java.time.LocalDate;

import javax.validation.constraints.NotEmpty;

import com.ranklands.pagoverbo.model.FraseComentario;

public class FraseComentarioRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@NotEmpty(message = "Preenchimento obrigatório")
	private String titulo;

	@NotEmpty(message = "Preenchimento obrigatório")
	private String textoComentario;

	private LocalDate dataComentario;

	@NotEmpty(message = "Preenchimento obrigatório")
	private String autorComentario;

	public FraseComentarioRequest() {

	}

	public FraseComentarioRequest(FraseComentario obj) {
		titulo = obj.getTitulo();
		textoComentario = obj.getTextoComentario();
		dataComentario = LocalDate.now();
		autorComentario = obj.getAutorComentario();
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getTextoComentario() {
		return textoComentario;
	}

	public void setTextoComentario(String textoComentario) {
		this.textoComentario = textoComentario;
	}

	public LocalDate getDataComentario() {
		return dataComentario;
	}

	public void setDataComentario(LocalDate dataComentario) {
		this.dataComentario = dataComentario;
	}

	public String getAutorComentario() {
		return autorComentario;
	}

	public void setAutorComentario(String autorComentario) {
		this.autorComentario = autorComentario;
	}

}