package com.ranklands.pagoverbo.request;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import com.ranklands.pagoverbo.model.Frase;

public class FraseRequest implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@NotEmpty(message = "Preenchimento obrigatório")
	private String texto;

	private Integer autorId;

	public FraseRequest() {

	}

	public FraseRequest(Frase obj) {
		texto = obj.getTexto();
		autorId = obj.getAutor().getId();
	}
	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public Integer getAutorId() {
		return autorId;
	}

	public void setAutorId(Integer autorId) {
		this.autorId = autorId;
	}

}