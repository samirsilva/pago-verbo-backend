package com.ranklands.pagoverbo.request;

import java.io.Serializable;

import com.ranklands.pagoverbo.model.Frase;

public class VotoFraseRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer autorId;

	private Integer votoPositivo;

	private Integer votoNegativo;

	public VotoFraseRequest(Frase frase) {

		votoPositivo = frase.getVotoPositivo();
		votoNegativo = frase.getVotoNegativo();
		autorId = frase.getAutor().getId();
	}

	public Integer getVotoPositivo() {
		return votoPositivo;
	}

	public void setVotoPositivo(Integer votoPositivo) {
		this.votoPositivo = votoPositivo;
	}

	public Integer getVotoNegativo() {
		return votoNegativo;
	}

	public void setVotoNegativo(Integer votoNegativo) {
		this.votoNegativo = votoNegativo;
	}

	public Integer getAutorId() {
		return autorId;
	}

	public void setAutorId(Integer autorId) {
		this.autorId = autorId;
	}

}
