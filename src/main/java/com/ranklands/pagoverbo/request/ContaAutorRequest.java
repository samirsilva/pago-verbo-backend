package com.ranklands.pagoverbo.request;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import com.ranklands.pagoverbo.model.ContaAutor;
import com.ranklands.pagoverbo.model.enums.TipoConta;

public class ContaAutorRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@NotEmpty(message = "Preenchimento obrigatório")
	private String nomeCompletoTitular;

	@NotEmpty(message = "Preenchimento obrigatório")
	private String banco;

	@NotEmpty(message = "Preenchimento obrigatório")
	private String agencia;

	@NotEmpty(message = "Preenchimento obrigatório")
	private String conta;

	private TipoConta tipoConta;

	@NotEmpty(message = "Preenchimento obrigatório")
	@Length(min = 11, max = 14, message = "O tamanho deve ser entre 11 a 14 caracteres")
	private String cpfCnpj;

	@NotEmpty(message = "Preenchimento obrigatório")
	private String digitoConta;

	private Integer autorId;

	public ContaAutorRequest() {

	}

	public ContaAutorRequest(ContaAutor obj) {
		nomeCompletoTitular = obj.getNomeCompletoTitular();
		banco = obj.getBanco();
		agencia = obj.getAgencia();
		conta = obj.getConta();
		tipoConta = obj.getTipoConta();
		cpfCnpj = obj.getCpfCnpj();
		digitoConta = obj.getDigitoConta();
		autorId = obj.getAutor().getId();

	}

	public String getNomeCompletoTitular() {
		return nomeCompletoTitular;
	}

	public void setNomeCompletoTitular(String nomeCompletoTitular) {
		this.nomeCompletoTitular = nomeCompletoTitular;
	}

	public String getAgencia() {
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public String getConta() {
		return conta;
	}

	public void setConta(String conta) {
		this.conta = conta;
	}

	public TipoConta getTipoConta() {
		return tipoConta;
	}

	public void setTipoConta(TipoConta tipoConta) {
		this.tipoConta = tipoConta;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getDigitoConta() {
		return digitoConta;
	}

	public void setDigitoConta(String digitoConta) {
		this.digitoConta = digitoConta;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public Integer getAutorId() {
		return autorId;
	}

	public void setAutorId(Integer autorId) {
		this.autorId = autorId;
	}

}