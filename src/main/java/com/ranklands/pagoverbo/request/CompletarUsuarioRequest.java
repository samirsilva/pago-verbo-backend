package com.ranklands.pagoverbo.request;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import com.ranklands.pagoverbo.model.Autor;
import com.ranklands.pagoverbo.service.validation.UsuarioUpdate;

@UsuarioUpdate
public class CompletarUsuarioRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@NotEmpty(message = "Preenchimento obrigatório")
	@Length(min = 5, max = 11, message = "O tamanho deve ser entre 5 a 11 caracteres")
	private Integer idade;

	@NotEmpty(message = "Preenchimento obrigatório")
	@Length(min = 5, max = 255, message = "O tamanho deve ser entre 5 a 255 caracteres")
	private String biografia;

	@NotEmpty(message = "Preenchimento obrigatório")
	private String email;
	
	public CompletarUsuarioRequest() {

	}

	public CompletarUsuarioRequest(Autor obj) {
		idade = obj.getIdade();
		biografia = obj.getBiografia();
		email = obj.getEmail();

	}

	public Integer getIdade() {
		return idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	public String getBiografia() {
		return biografia;
	}

	public void setBiografia(String biografia) {
		this.biografia = biografia;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
