package com.ranklands.pagoverbo.request;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.constraints.NotEmpty;

import com.ranklands.pagoverbo.model.SolicitacaoSaque;

public class SolicitacaoSaqueRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@NotEmpty(message = "Preenchimento obrigatório")
	private BigDecimal valorSaque;

	public SolicitacaoSaqueRequest() {

	}

	public SolicitacaoSaqueRequest(SolicitacaoSaque obj) {

		valorSaque = obj.getValorSaque();
	}

	public BigDecimal getValorSaque() {
		return valorSaque;
	}

	public void setValorSaque(BigDecimal valorSaque) {
		this.valorSaque = valorSaque;
	}

}