package com.ranklands.pagoverbo.request;

import java.io.Serializable;

import com.ranklands.pagoverbo.model.enums.Status;

public class AlterarSaqueRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	private Status status;
	
	private String motivoAlteracaoStatus;

	public AlterarSaqueRequest() {

	}
	
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getMotivoAlteracaoStatus() {
		return motivoAlteracaoStatus;
	}

	public void setMotivoAlteracaoStatus(String motivoAlteracaoStatus) {
		this.motivoAlteracaoStatus = motivoAlteracaoStatus;
	}

}