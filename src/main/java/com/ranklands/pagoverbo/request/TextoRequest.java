package com.ranklands.pagoverbo.request;

import java.io.Serializable;
import java.time.LocalDate;

import javax.validation.constraints.NotEmpty;

import com.ranklands.pagoverbo.model.Texto;

public class TextoRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	@NotEmpty(message = "Preenchimento obrigatório")
	private String texto;

	private LocalDate dataLancamento;

	private Integer votoPositivo;

	private Integer votoNegativo;

	@NotEmpty(message = "Preenchimento obrigatório")
	private Integer autorId;
	
	@NotEmpty(message = "Preenchimento obrigatório")
	private String categoria;

	public TextoRequest() {

	}

	public TextoRequest(Texto obj) {
		texto = obj.getTexto();
		dataLancamento = LocalDate.now();
		votoPositivo = obj.getVotoPositivo();
		votoNegativo = obj.getVotoNegativo();
		autorId = obj.getAutor().getId();
		categoria = obj.getCategoria();
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public LocalDate getDataLancamento() {
		return dataLancamento;
	}

	public void setDataLancamento(LocalDate dataLancamento) {
		this.dataLancamento = dataLancamento;
	}

	public Integer getVotoPositivo() {
		return votoPositivo;
	}

	public void setVotoPositivo(Integer votoPositivo) {
		this.votoPositivo = votoPositivo;
	}

	public Integer getVotoNegativo() {
		return votoNegativo;
	}

	public void setVotoNegativo(Integer votoNegativo) {
		this.votoNegativo = votoNegativo;
	}

	public Integer getAutorId() {
		return autorId;
	}

	public void setAutorId(Integer autorId) {
		this.autorId = autorId;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

}