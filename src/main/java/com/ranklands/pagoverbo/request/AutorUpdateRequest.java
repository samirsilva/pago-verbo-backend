package com.ranklands.pagoverbo.request;

import java.io.Serializable;

import javax.validation.constraints.Email;

import com.ranklands.pagoverbo.model.Autor;

public class AutorUpdateRequest implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;

	private String nome;

	private String sobrenome;

	private Integer idade;

	@Email(message = "Email inválido")
	private String email;

	private String biografia;
	
	private Integer votoPositivo;
	
	private Integer votoNegativo;

	public AutorUpdateRequest() {

	}

	public AutorUpdateRequest(Autor obj) {
		id = obj.getId();
		nome = obj.getNome();
		sobrenome = obj.getSobrenome();
		email = obj.getEmail();
		idade = obj.getIdade();
		biografia = obj.getBiografia();
		votoPositivo = obj.getVotoPositivo();
		votoNegativo = obj.getVotoNegativo();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public Integer getIdade() {
		return idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	public String getBiografia() {
		return biografia;
	}

	public void setBiografia(String biografia) {
		this.biografia = biografia;
	}

	public Integer getVotoPositivo() {
		return votoPositivo;
	}

	public void setVotoPositivo(Integer votoPositivo) {
		this.votoPositivo = votoPositivo;
	}

	public Integer getVotoNegativo() {
		return votoNegativo;
	}

	public void setVotoNegativo(Integer votoNegativo) {
		this.votoNegativo = votoNegativo;
	}

}
