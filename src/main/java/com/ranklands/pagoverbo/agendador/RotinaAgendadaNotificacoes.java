package com.ranklands.pagoverbo.agendador;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.ranklands.pagoverbo.model.Autor;
import com.ranklands.pagoverbo.model.NotificacaoAutor;
import com.ranklands.pagoverbo.repository.AutorRepository;
import com.ranklands.pagoverbo.repository.NotificacaoAutorRepository;
import com.ranklands.pagoverbo.service.exception.DataIntegrityException;

@Component
@EnableScheduling
public class RotinaAgendadaNotificacoes {
	public static final int MIN = 1000 * 60;
	@Autowired
	private AutorRepository autorRepository;
	
	@Autowired
	private NotificacaoAutorRepository notificacaoRepository;

    //@Scheduled(fixedRate = MIN)
	@Scheduled(cron = "0 0 0 1 1-12 ?")
	public void scheduleFutureTask() {

		LocalDate dataRotina = LocalDate.now();

		List<Autor> autores = this.autorRepository.findAllByOrderByVotoPositivoDesc();
		for (Autor autor : autores) {
			for(NotificacaoAutor not : autor.getNotificacoes()) {
				if(!not.getMesReferencia().equals(dataRotina.getMonthValue())) {

					this.delete(not.getId());
				}				
			}
		}
		System.out.println("Agendador de verificacao de Notificações defasadas: " + dataRotina);
	}
	
	public void delete(Integer id) {		
		try {
			Optional<NotificacaoAutor> notificacao = this.notificacaoRepository.findById(id);
						
			this.notificacaoRepository.delete(notificacao.get());
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("não é possível excluir por que há entidades relacionadas");
		}
	}
}
