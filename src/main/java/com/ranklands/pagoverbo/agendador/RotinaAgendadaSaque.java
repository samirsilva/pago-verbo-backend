package com.ranklands.pagoverbo.agendador;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.ranklands.pagoverbo.model.Autor;
import com.ranklands.pagoverbo.model.NotificacaoAutor;
import com.ranklands.pagoverbo.model.enums.Status;
import com.ranklands.pagoverbo.repository.AutorRepository;
import com.ranklands.pagoverbo.repository.NotificacaoAutorRepository;
import com.ranklands.pagoverbo.repository.SolicitacaoSaqueRepository;

@Component
@EnableScheduling
public class RotinaAgendadaSaque {
	public static final int MIN = 1000 * 60;
	
	@Autowired
	private AutorRepository autorRepository;

	@Autowired
	private NotificacaoAutorRepository notificacaoAutorRepository;
	
	@Autowired
	private SolicitacaoSaqueRepository saqueRepository;
	
    //@Scheduled(fixedRate = MIN)
    @Scheduled(cron = "0 0 0-23 * * *")
	public void scheduleFutureTask() {

		LocalDate dataRotina = LocalDate.now();

		List<Autor> autores = this.autorRepository.findAllByOrderByVotoPositivoDesc();
		NotificacaoAutor notificacao = new NotificacaoAutor();
	
		for (Autor autor : autores) {
			if(autor.getSaques() != null || !autor.getSaques().isEmpty()) {
				autor.getSaques().forEach(saque -> {
					if(saque.getStatus() == Status.TRANSFERIDO && !saque.getNotificacao()) {
						notificacao.setTitulo("Financeiro");
						notificacao.setTexto(autor.getNome() +", o status do seu saque foi alterado para transferido, em até 48 horas úteis o valor de " 
						+ saque.getValorSaque() + " será creditado na conta " + autor.getContaAutor().getBanco() + " cadastrada.");
						notificacao.setAutor(autor);
						notificacao.setData(LocalDateTime.now());
						notificacao.setMesReferencia(LocalDateTime.now().getMonthValue());
						
					}else if(saque.getStatus() == Status.RECUSADO && !saque.getNotificacao()) {
						notificacao.setTitulo("Financeiro");
						notificacao.setTexto(autor.getNome() +", o seu saque foi recusado, Motivo: " + saque.getMotivoAlteracaoStatus() +
								" O valor do saque foi extornado, Saldo atual: " + autor.getSaldo());
						notificacao.setAutor(autor);
						notificacao.setData(LocalDateTime.now());
						notificacao.setMesReferencia(LocalDateTime.now().getMonthValue());						
					}
					
					if(notificacao.getAutor() != null) {
						saque.setNotificacao(true);
						
						this.saqueRepository.save(saque);
						this.notificacaoAutorRepository.save(notificacao);												
					}
				});
			}
		}
		System.out.println("Agendador de verificacao de Status de saque executado na data: " + dataRotina);
	}
}
