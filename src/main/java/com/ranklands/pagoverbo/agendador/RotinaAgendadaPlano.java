package com.ranklands.pagoverbo.agendador;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.ranklands.pagoverbo.model.Autor;
import com.ranklands.pagoverbo.model.NotificacaoAutor;
import com.ranklands.pagoverbo.repository.AutorRepository;
import com.ranklands.pagoverbo.repository.NotificacaoAutorRepository;

@Component
@EnableScheduling
public class RotinaAgendadaPlano {
	
	@Autowired
	private AutorRepository autorRepository;
	
	@Autowired
	private NotificacaoAutorRepository notificacaoAutor;
		
    @Scheduled(cron = "0 0 * * * *")
    public void scheduleFutureTask() {
	
    	LocalDateTime dataRotina = LocalDateTime.now();
    	
    	List<Autor> autores = this.autorRepository.findAll();
    	for (Autor autor : autores) {
			if (autor.getPlano()) {
				
				if (autor.getDataMonitoramentoPlano().getDayOfMonth() != dataRotina.getDayOfMonth()) {
					autor.setDiaPlano(autor.getDiaPlano() - 1);
					
					if(autor.getDiaPlano() == 0) {
						autor.setPlano(false);
						
						NotificacaoAutor notificacao = new NotificacaoAutor();
						
						notificacao.setTitulo("Sistema");
						notificacao.setTexto(autor.getNome() +", seu plano encerrou todas suas vantagens serão temporáriamente bloqueadas, Renove e ganhe dinheiro!");
						notificacao.setAutor(autor);
						notificacao.setData(LocalDateTime.now());
						notificacao.setMesReferencia(LocalDateTime.now().getMonthValue());
						
						this.notificacaoAutor.save(notificacao);
					}
				}
				autor.setDataMonitoramentoPlano(dataRotina);
	    		this.autorRepository.save(autor);				
			}
    	}
    	
    	System.out.println("Agendador de Plano executado na data: " + dataRotina);
    }
}
