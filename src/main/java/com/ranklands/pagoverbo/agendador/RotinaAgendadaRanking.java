package com.ranklands.pagoverbo.agendador;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.ranklands.pagoverbo.model.Autor;
import com.ranklands.pagoverbo.repository.AutorRepository;

@Component
@EnableScheduling
public class RotinaAgendadaRanking {

	@Autowired
	private AutorRepository autorRepository;

    @Scheduled(cron = "0 0 0-23 * * *")
	public void scheduleFutureTask() {

		LocalDate dataRotina = LocalDate.now();

		List<Autor> autores = this.autorRepository.findAllByOrderByVotoPositivoDesc();
		int i = 1;
		for (Autor autor : autores) {
			if (autor.getVotoPositivo() > 0) {
				autor.setPosicao(i);
			}	
			this.autorRepository.save(autor);
			i++;
		}
		System.out.println("Agendador de verificacao de Ranking executado na data: " + dataRotina);
	}
}
