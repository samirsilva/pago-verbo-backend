package com.ranklands.pagoverbo.agendador;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.ranklands.pagoverbo.model.Autor;
import com.ranklands.pagoverbo.model.NotificacaoAutor;
import com.ranklands.pagoverbo.model.VotoAutorFrase;
import com.ranklands.pagoverbo.model.VotoAutorTexto;
import com.ranklands.pagoverbo.repository.AutorRepository;
import com.ranklands.pagoverbo.repository.NotificacaoAutorRepository;
import com.ranklands.pagoverbo.repository.VotoFraseRepository;
import com.ranklands.pagoverbo.repository.VotoTextoRepository;
import com.ranklands.pagoverbo.service.exception.DataIntegrityException;

@Component
@EnableScheduling
public class RotinaAgendadaVotos {
	public static final int MIN = 1000 * 60;

	@Autowired
	private AutorRepository autorRepository;
	
	@Autowired
	private NotificacaoAutorRepository notificacaoAutor;
	
	@Autowired
	private VotoFraseRepository votoFraseRepository;
	
	@Autowired
	private VotoTextoRepository votoTextoRepository;
	
    //@Scheduled(fixedRate = MINUTO)
	@Scheduled(cron = "0 0 0 1 1-12 ?")
    public void scheduleFutureTask() {
    	
    	LocalDateTime dataRotina = LocalDateTime.now();

    	List<Autor> autores = this.autorRepository.findAll();
    	for (Autor autor : autores) {
    		if(autor.getMesReferenciaVoto() != dataRotina.getMonthValue()) {
    			
    			autor.setMesReferenciaVoto(dataRotina.getMonthValue());
    			autor.setVotoNegativo(0);
    			autor.setVotoPositivo(0);
    			
    			autor.getTextos().forEach(texto ->{
    				if(texto.getMesReferenciaVoto() != dataRotina.getMonthValue()) {
    					texto.setVotoPositivo(0);
        				texto.setVotoNegativo(0);    					
        				texto.setMesReferenciaVoto(dataRotina.getMonthValue());
    				}
    			});
    			
    			autor.getFrases().forEach(frase -> {
    				if(frase.getMesReferenciaVoto() != dataRotina.getMonthValue()) {
    					frase.setVotoPositivo(0);
    					frase.setVotoNegativo(0);    					
    					frase.setMesReferenciaVoto(dataRotina.getMonthValue());   					
    				}
    			});
    			
    			for(VotoAutorFrase votosFrases : autor.getVotosAutorFrases()) {
    				if(votosFrases.getDataLancamento().getMonthValue() != dataRotina.getMonthValue()) {
        				deleteVotoFrase(votosFrases.getId());
    				}   				
    			}

    			for(VotoAutorTexto votosTextos : autor.getVotosAutorTextos()) {
    				if(votosTextos.getDataLancamento().getMonthValue() != dataRotina.getMonthValue()) {
        				deleteVotoTexto(votosTextos.getId());
    					
    				}    				
    			}

				NotificacaoAutor notificacao = new NotificacaoAutor();

				notificacao.setTitulo("Sistema");
				notificacao.setTexto(autor.getNome() +", mais uma temporada foi encerrada, os seus votos foram todos resetados, bons textos!");
				notificacao.setAutor(autor);
				notificacao.setData(LocalDateTime.now());
				notificacao.setMesReferencia(LocalDateTime.now().getMonthValue());
				
				this.notificacaoAutor.save(notificacao);
    		}
    		
    		this.autorRepository.save(autor);
    	}

    	
    	System.out.println("Agendador de Votos executado na data: " + dataRotina);
    }
    
	public void deleteVotoFrase(Integer id) {		
		try {
			Optional<VotoAutorFrase> votosFrases = this.votoFraseRepository.findById(id);
						
			this.votoFraseRepository.delete(votosFrases.get());
			
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("não é possível excluir por que há entidades relacionadas");
		}
	}
	
	public void deleteVotoTexto(Integer id) {		
		try {
			Optional<VotoAutorTexto> votosTextos = this.votoTextoRepository.findById(id);
						
			this.votoTextoRepository.delete(votosTextos.get());
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("não é possível excluir por que há entidades relacionadas");
		}
	}
}
