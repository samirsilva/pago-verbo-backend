package com.ranklands.pagoverbo.service;

import java.awt.image.BufferedImage;
import java.math.BigDecimal;
import java.net.URI;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.ranklands.pagoverbo.model.Autor;
import com.ranklands.pagoverbo.model.ContaAutor;
import com.ranklands.pagoverbo.model.ExtornoValorBanco;
import com.ranklands.pagoverbo.model.NotificacaoAutor;
import com.ranklands.pagoverbo.model.Titulo;
import com.ranklands.pagoverbo.model.enums.Status;
import com.ranklands.pagoverbo.model.enums.TipoBanco;
import com.ranklands.pagoverbo.model.enums.TipoConta;
import com.ranklands.pagoverbo.model.enums.TipoUsuario;
import com.ranklands.pagoverbo.model.parametro.ParametroBanco;
import com.ranklands.pagoverbo.model.parametro.ParametroExperienciaNivel;
import com.ranklands.pagoverbo.repository.AutorRepository;
import com.ranklands.pagoverbo.repository.ContaAutorRepository;
import com.ranklands.pagoverbo.repository.ExtornoValorBancoRepository;
import com.ranklands.pagoverbo.repository.NotificacaoAutorRepository;
import com.ranklands.pagoverbo.repository.parametro.ParametroBancoRepository;
import com.ranklands.pagoverbo.repository.parametro.ParametroExperienciaNivelRepository;
import com.ranklands.pagoverbo.request.AtivarPlanoRequest;
import com.ranklands.pagoverbo.request.AutorUpdateRequest;
import com.ranklands.pagoverbo.request.ContaAutorRequest;
import com.ranklands.pagoverbo.request.NovoUsuarioRequest;
import com.ranklands.pagoverbo.security.UserSS;
import com.ranklands.pagoverbo.service.email.EmailService;
import com.ranklands.pagoverbo.service.exception.AuthorizationException;
import com.ranklands.pagoverbo.service.exception.DataIntegrityException;
import com.ranklands.pagoverbo.service.exception.ObjectNotFoundException;
import com.ranklands.pagoverbo.service.gameficacao.GameficacaoService;

import javassist.NotFoundException;

@Service
public class AutorService {

	@Autowired
	private AutorRepository autorRepository;
	
	@Autowired
	private ContaAutorRepository contaAutorRepository;
	
	@Autowired
	private ParametroBancoRepository bancoRepository;
	
	@Autowired
	private ParametroExperienciaNivelRepository parametroExperienciaNivelRepository;
	
	@Autowired
	private NotificacaoAutorRepository notificacaoRepository;
	
	@Autowired
	private GameficacaoService gameficacao;

	@Autowired
	private BCryptPasswordEncoder pe;
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	private S3Service s3Service;

	@Autowired
	private ImageService imageService;
		
	@Value("${img.prefix.client.profile}")
	private String prefix;
	
	@Value("${img.profile.size}")
	private Integer size;
	
	@Autowired
	private ExtornoValorBancoRepository extornoValorBancoRepository;
	
	public Autor find(Integer id) {

		UserSS user = UserService.authenticated();
		if (user == null || !user.hasHole(TipoUsuario.USUARIO) && !id.equals(user.getId())) {
			throw new AuthorizationException("Acesso negado");
		}

		Optional<Autor> obj = this.autorRepository.findById(id);

		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Usuário não encontrado! Id: " + id + ", Tipo: " + Autor.class.getName()));
	}

	public ContaAutor findConta(Integer id) {

		UserSS user = UserService.authenticated();
		if (user == null || !user.hasHole(TipoUsuario.USUARIO) && !id.equals(user.getId())) {
			throw new AuthorizationException("Acesso negado");
		}

		Optional<ContaAutor> obj = this.contaAutorRepository.findById(id);

		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Usuário não encontrado! Id: " + id + ", Tipo: " + ContaAutor.class.getName()));
	}
	
	@Transactional
	public Autor insert(Autor obj) {

		obj.setId(null);
		obj = this.autorRepository.save(obj);
		this.emailService.sendOrderConfirmationHtmlEmail(obj);
		return obj;
	}

	public Autor update(Autor obj) {
		Autor novoAutor = find(obj.getId());

		this.updateData(novoAutor, obj);
		
		if (novoAutor.getPerfilCompleto() < 100) {
			if (novoAutor.getIdade() != null) {
				
				if(!novoAutor.getIdadeOk()) {
					novoAutor.setPerfilCompleto(novoAutor.getPerfilCompleto() + 15);
					novoAutor.setIdadeOk(true);
				}
			}
			if (novoAutor.getBiografia() != null) {
				if(!novoAutor.getBiografiaOk()) {

					novoAutor.setPerfilCompleto(novoAutor.getPerfilCompleto() + 15);
					novoAutor.setBiografiaOk(true);
				}

			}
		}
		
		
		return this.autorRepository.save(novoAutor);
	}
	
	@Transactional
	public ContaAutor insertContaAutor(ContaAutor conta) {
		Autor autor = find(conta.getAutor().getId());

		if(autor.getIdadeOk() && autor.getBiografiaOk()) {
			conta.setId(null);
			conta = this.contaAutorRepository.save(conta);
			
			autor.setPerfilCompleto(autor.getPerfilCompleto() + 20);
			
			if(autor.getPerfilCompleto() == 100) {
				NotificacaoAutor notificacao = new NotificacaoAutor();
				
				notificacao.setTitulo("Sistema");
				notificacao.setTexto("Parabéns, você acabou de completar 100% do seu perfil e ganhou um titulo exclusivo");
				notificacao.setData(LocalDateTime.now());
				notificacao.setMesReferencia(LocalDateTime.now().getMonthValue());
				notificacao.setAutor(autor);
				
				autor.getNotificacoes().add(notificacao);

				Titulo titulo = new Titulo();
				List<ParametroExperienciaNivel> exps = this.parametroExperienciaNivelRepository.findAll();
								
				titulo.setNome("Eu Sou de verdade");
				titulo.setDescricao("Parabéns você completou 100% o seu perfil, vamos ganhar uma grana!");
				titulo.setAutor(autor);
				
				autor.getTitulos().add(titulo);
				
				autor.setExp(autor.getExp() + 150);
				
				this.gameficacao.verificaNivel(exps, autor);
				
				autor.setContaAutor(conta);
			}
			
			this.autorRepository.save(autor);			
		}else {
			throw new IllegalArgumentException("Para inserir uma conta, é necessário ter concluido o preenchimento da sua idade e breve biografia");
		}

		
		return conta;
	}
	
	public ContaAutor updateContaAutor(ContaAutor conta) {
		ContaAutor newConta = this.findConta(conta.getId());
		
		this.updateDataConta(newConta, conta);
		
		return this.contaAutorRepository.save(newConta);
	}
	public void delete(Integer id) {		
		try {
			Optional<ParametroBanco> banco = this.bancoRepository.findById(1);
			Optional<Autor> autor = this.autorRepository.findById(id);
			ExtornoValorBanco extorno = new ExtornoValorBanco();
			BigDecimal valorSaquePendente = new BigDecimal("0"); 
			
			banco.get().setSaldo(banco.get().getSaldo().add(autor.get().getSaldo()));
			
			autor.get().getSaques().forEach(saque -> {
				if(saque.getStatus().equals(Status.PENDENTE)) {
					banco.get().setSaldo(banco.get().getSaldo().add(saque.getValorSaque()));
					
					valorSaquePendente.add(saque.getValorSaque());
				}
			});
			
			extorno.setNome(autor.get().getNome());
			extorno.setSobrenome(autor.get().getSobrenome());
			extorno.setEmail(autor.get().getEmail());
			extorno.setValor(autor.get().getSaldo().add(valorSaquePendente));
			extorno.setDataExtorno(LocalDate.now());
			
			this.extornoValorBancoRepository.save(extorno);
			this.autorRepository.deleteById(id);
			this.bancoRepository.save(banco.get());
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("não é possível excluir por que há entidades relacionadas");
		}
	}

	public List<Autor> findAll() {
		return this.autorRepository.findAllByOrderByVotoPositivoDesc();
	}

	public Page<Autor> findPage(Integer page, Integer linesPage, String orderBy, String direction) {
		PageRequest pageRequest = PageRequest.of(page, linesPage, Direction.valueOf(direction), orderBy);

		return this.autorRepository.findAll(pageRequest);
	}

	public Autor findByEmail(String email) {
		UserSS user = UserService.authenticated();
		if (user == null || !user.hasHole(TipoUsuario.ADMIN) && !email.equals(user.getUsername())) {
			throw new AuthorizationException("Acesso negado");
		}
	
		Autor obj = this.autorRepository.findByEmail(email);
		if (obj == null) {
			throw new ObjectNotFoundException(
					"Objeto não encontrado! Id: " + user.getId() + ", Tipo: " + Autor.class.getName());
		}
		return obj;
	}
	
	public Autor fromDTO(AutorUpdateRequest userReq) {
		
		return new Autor(	userReq.getId(), userReq.getNome(),userReq.getSobrenome(), userReq.getIdade(), 
				userReq.getEmail(), userReq.getBiografia());
	}

	public Autor fromDTO(NovoUsuarioRequest userReq) {
		return new Autor(	null, userReq.getNome(),userReq.getSobrenome(), null, userReq.getEmail(), 
							userReq.getCpf(), pe.encode(userReq.getSenha()), false,
							BigDecimal.ZERO, 0, 0, null, 0, 0,null, 50,0,false,false,0,0, 
							LocalDate.now().getMonthValue(), LocalDate.now(), LocalDateTime.now(),null);
	}

	public ContaAutor fromDTO(ContaAutorRequest request) throws NotFoundException {
		Optional<Autor> autor = this.autorRepository.findById(request.getAutorId());
		if(autor.get() == null) {
			throw new NotFoundException("Desculpe, não conseguimos encontrar esse autor");
		}
		
		ContaAutor conta = this.validaDadosbancarios(request);
		
		return conta;
	}
	
	private void updateData(Autor novoAutor, Autor autor) {
		novoAutor.setNome(autor.getNome());
		novoAutor.setSobrenome(autor.getSobrenome());
		novoAutor.setIdade(autor.getIdade());
		novoAutor.setEmail(autor.getEmail());
		novoAutor.setBiografia(autor.getBiografia());
	}
	
	private void updateDataConta(ContaAutor newConta, ContaAutor conta) {
		newConta.setAgencia(conta.getAgencia());
		newConta.setNomeCompletoTitular(conta.getNomeCompletoTitular());
		newConta.setCpfCnpj(conta.getCpfCnpj());
		newConta.setConta(conta.getConta());
		newConta.setOperacao(conta.getOperacao());
		newConta.setTipoConta(conta.getTipoConta());
		newConta.setAutor(conta.getAutor());
		newConta.setOperacao(conta.getOperacao());
		newConta.setDigitoAgencia(conta.getDigitoAgencia());

		
		if(conta.getNumeroBanco() != null) {
			newConta.setNumeroBanco(conta.getNumeroBanco());
		}
		if(conta.getBanco() != null) {
			newConta.setBanco(conta.getBanco());
		}		
		
		if(conta.getDigitoBanco() != null) {
			newConta.setDigitoBanco(conta.getDigitoBanco());
		}		
		
		if(conta.getDigitoConta() != null) {
			newConta.setDigitoConta(conta.getDigitoConta());
		}
		
	}
	
	public ContaAutor validaDadosbancarios(ContaAutorRequest request) {
		
		ContaAutor conta = new ContaAutor();
		
        this.verificaTipoConta(request, conta, false);
		
		if (request.getAgencia() != null) {
		      conta.setAgencia(request.getAgencia().substring(0, 4));
		}
    
		if (request.getConta() != null) {
			conta.setDigitoConta(request.getConta().substring(request.getConta().length() - 1, request.getConta().length()));    
		}
		
		if (request.getBanco() != null) {
				
			String nomebanco = request.getBanco().toUpperCase().replaceAll(" ", "_");
			
		      switch (TipoBanco.valueOf(TipoBanco.class, nomebanco)) {
		      case BANCO_DO_BRASIL:
		        if (request.getAgencia()!= null) {
		          conta.setDigitoAgencia(request.getAgencia().substring(request.getAgencia().length() - 1, request.getAgencia().length()));
		        }

		        if (request.getConta() != null) {
		          conta.setConta(request.getConta().substring(0, 8));
		        }

		        conta.setBanco(nomebanco);
		        conta.setNumeroBanco(TipoBanco.BANCO_DO_BRASIL.getValor());
		        conta.setDigitoBanco("9");
		        conta.setDigitoConta(request.getDigitoConta());

		        break;
		      case SANTANDER:
		        conta.setDigitoAgencia(null);

		        if (request.getConta() != null) {
		          conta.setConta(request.getConta().substring(0, 8));
		        }

		        conta.setBanco(nomebanco);
		        conta.setNumeroBanco(TipoBanco.SANTANDER.getValor());
		        conta.setDigitoBanco("7");
		        conta.setDigitoConta(request.getDigitoConta());

		        break;
		      case CAIXA_ECONÔMICA:
		        conta.setDigitoAgencia(null);

		        if (request.getConta() != null) {
		          conta.setConta(request.getConta().substring(3, 11));
		        }
		        this.verificaTipoConta(request, conta, true);

				conta.setBanco(nomebanco);
		        conta.setNumeroBanco(TipoBanco.CAIXA_ECONÔMICA.getValor());
		        conta.setDigitoBanco("0");
		        conta.setDigitoConta(request.getDigitoConta());
		        conta.setOperacao(request.getConta().substring(0, 3));

		        break;
		      case BRADESCO:
		        if (request.getAgencia() != null) {
		          conta.setDigitoAgencia(request.getAgencia().substring(request.getAgencia() .length() - 1, request.getAgencia() .length()));
		        }

		        if (request.getConta() != null) {
		          conta.setConta(request.getConta().substring(0, 7));
		        }

		        conta.setBanco(nomebanco);
		        conta.setNumeroBanco(TipoBanco.BRADESCO.getValor());
		        conta.setDigitoBanco("2");
		        conta.setDigitoConta(request.getDigitoConta());

		        break;
		      case ITAU:
		        conta.setDigitoAgencia(null);

		        if (request.getConta() != null) {
		          conta.setConta(request.getConta().substring(0, 5));
		        }

		        conta.setBanco(nomebanco);
		        conta.setNumeroBanco(TipoBanco.ITAU.getValor());
		        conta.setDigitoBanco("7");
		        conta.setDigitoConta(request.getDigitoConta());
		        break;
		      case BANRISUL:
		        conta.setDigitoAgencia(null);

		        if (request.getConta() != null) {
		          conta.setConta(request.getConta().substring(0, 9));
		        }

		        conta.setBanco(nomebanco);
		        conta.setNumeroBanco(TipoBanco.BANRISUL.getValor());
		        conta.setDigitoBanco("8");
		        conta.setDigitoConta(request.getDigitoConta());

		        break;
		      case SICREDI:
		        conta.setDigitoAgencia(null);

		        if (request.getConta() != null) {
		          conta.setConta(request.getConta().substring(0, 5));
		        }

		        conta.setBanco(nomebanco);
		        conta.setNumeroBanco(TipoBanco.SICREDI.getValor());
		        conta.setDigitoBanco("0");
		        conta.setDigitoConta(request.getDigitoConta());

		        break;
		      case SICOOB:
		        conta.setDigitoAgencia(null);

		        if (request.getConta() != null) {
		          conta.setConta(request.getConta().substring(0, 9));
		        }

		        conta.setBanco(nomebanco);
		        conta.setNumeroBanco(TipoBanco.SICOOB.getValor());
		        conta.setDigitoBanco("0");
		        conta.setDigitoConta(request.getDigitoConta());

		        break;
		      case INTER:
		        conta.setDigitoAgencia(null);

		        if (request.getConta() != null) {
		          conta.setConta(request.getConta().substring(0, 9));
		        }
		        conta.setBanco(nomebanco);
		        conta.setNumeroBanco(TipoBanco.INTER.getValor());
		        conta.setDigitoBanco("9");
		        conta.setDigitoConta(request.getDigitoConta());

		        break;
		      case BRB:
		        conta.setDigitoAgencia(null);

		        if (request.getConta() != null) {
		          conta.setConta(request.getConta().substring(0, 9));
		        }

		        conta.setBanco(nomebanco);
		        conta.setNumeroBanco(TipoBanco.BRB.getValor());
		        conta.setDigitoBanco("1");
		        conta.setDigitoConta(request.getDigitoConta());

		        break;
		      default:
		        throw new IllegalArgumentException("Banco inválido!");
		 
		      }
		    
		}
		
		if(request.getNomeCompletoTitular() != null) {
			conta.setNomeCompletoTitular(request.getNomeCompletoTitular());
		}
		if(request.getCpfCnpj() != null) {
			conta.setCpfCnpj(request.getCpfCnpj());
		}
		if(request.getAutorId() != null) {
			Autor autor = find(request.getAutorId());
			conta.setAutor(autor);
		}
		    
		return conta;
	}
	
	public void verificaTipoConta(ContaAutorRequest request, ContaAutor conta, Boolean gatilho) {
		
		if (!gatilho) {
			if (request.getTipoConta() != null) {
				switch (request.getTipoConta()) {

				case POUPANÇA:
					conta.setTipoConta(TipoConta.POUPANÇA);
					break;

				default:
					conta.setTipoConta(TipoConta.CORRENTE);
					break;

				}
			}
		}else {
			if (request.getTipoConta() != null) {
				switch (request.getTipoConta()) {

				case POUPANÇA:
					conta.setTipoConta(TipoConta.POUPANÇA);
					conta.setOperacao("013");

					break;

				default:
					conta.setTipoConta(TipoConta.CORRENTE);
					conta.setOperacao("001");
					break;

				}
			}
		}
	}
	
	public URI uploadProfilePicture(MultipartFile multipartFile) {
		UserSS user = UserService.authenticated();
		if (user == null) {
			throw new AuthorizationException("Acesso negado");
		}
				
		BufferedImage jpgImage = imageService.getJpgImageFromFile(multipartFile);
		jpgImage = imageService.cropSquare(jpgImage);
		jpgImage = imageService.resize(jpgImage, size);
		
		String fileName = prefix + user.getId() + ".jpg";
		
		return this.s3Service.uploadFile(imageService.getInputStream(jpgImage, "jpg"), fileName, "image");
	}

	public void ativarPlanoPago(AtivarPlanoRequest request, Integer id) {
		Autor autor = find(id);
		NotificacaoAutor notificacao = new NotificacaoAutor();
		
		autor.setPlano(true);
		autor.setDiaPlano(request.getDiasPlano());
		
		notificacao.setTitulo("Plano");
		notificacao.setData(LocalDateTime.now());
		notificacao.setMesReferencia(LocalDateTime.now().getMonthValue());
		notificacao.setTexto(autor.getNome() +", agora você é um usuário exclusivo! Espero que você tenha a melhor experiência com a nossa assínatura.");
		notificacao.setAutor(autor);
		
		this.autorRepository.save(autor);
		this.notificacaoRepository.save(notificacao);
	}
}
