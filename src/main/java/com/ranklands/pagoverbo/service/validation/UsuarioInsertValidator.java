package com.ranklands.pagoverbo.service.validation;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.ranklands.pagoverbo.controller.exception.FieldMessage;
import com.ranklands.pagoverbo.model.Autor;
import com.ranklands.pagoverbo.repository.AutorRepository;
import com.ranklands.pagoverbo.request.NovoUsuarioRequest;

public class UsuarioInsertValidator implements ConstraintValidator<UsuarioInsert, NovoUsuarioRequest> {
	
	@Autowired
	private AutorRepository repo;
	
	@Override
	public void initialize(UsuarioInsert ann) {
	}

	@Override
	public boolean isValid(NovoUsuarioRequest objRequest, ConstraintValidatorContext context) {
		List<FieldMessage> list = new ArrayList<>();
		
		Autor aux = repo.findByEmail(objRequest.getEmail());
		if(aux != null) {
			list.add(new FieldMessage("email", "Email já existente"));
		}
		Autor aux2 = repo.findByCpf(objRequest.getCpf());
		if(aux2 != null) {
			list.add(new FieldMessage("cpf", "CPF já existente"));

		}
		
		for (FieldMessage e : list) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(e.getMessage()).addPropertyNode(e.getFieldName())
					.addConstraintViolation();
		}
		return list.isEmpty();
	}
}
