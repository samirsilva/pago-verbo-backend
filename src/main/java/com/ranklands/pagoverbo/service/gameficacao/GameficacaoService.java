package com.ranklands.pagoverbo.service.gameficacao;

import java.util.List;

import com.ranklands.pagoverbo.model.Autor;
import com.ranklands.pagoverbo.model.Frase;
import com.ranklands.pagoverbo.model.Texto;
import com.ranklands.pagoverbo.model.parametro.ParametroBanco;
import com.ranklands.pagoverbo.model.parametro.ParametroExperienciaNivel;
import com.ranklands.pagoverbo.model.parametro.ParametroGameficacao;
import com.ranklands.pagoverbo.model.parametro.ParametroTitulo;

import javassist.NotFoundException;

public interface GameficacaoService {

	public void preparaAutorParaGameficacao(Frase frase, Texto texto, Boolean insercaoVoto) throws NotFoundException;
	
	public void verificacoesObjetos(Autor autor, ParametroGameficacao gameficacao, List<ParametroExperienciaNivel> exps, List<ParametroTitulo> titulos) throws NotFoundException;
	
	public void verificacoesBanco(ParametroBanco banco, Autor autor, Frase frase, Texto texto, Boolean insercaoVoto, Boolean fraseTexto);
	
	public void verificacoesLimitacoesPlano(Autor autor, Boolean insercaoVoto, Boolean fraseTexto);
	
	public void insereExp(List<ParametroExperienciaNivel> exps, ParametroGameficacao gameficacao, Autor autor, Boolean insercaoVoto, Boolean fraseTexto);
	
	public void verificaTitulo(List<ParametroTitulo> titulos, ParametroGameficacao gameficacao, Autor autor, Boolean insercaoVoto, Boolean fraseTexto);
	
	public void verificaNivel(List<ParametroExperienciaNivel> exps, Autor autor);
}
