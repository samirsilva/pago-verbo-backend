package com.ranklands.pagoverbo.service.gameficacao;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ranklands.pagoverbo.model.Autor;
import com.ranklands.pagoverbo.model.Frase;
import com.ranklands.pagoverbo.model.NotificacaoAutor;
import com.ranklands.pagoverbo.model.Texto;
import com.ranklands.pagoverbo.model.Titulo;
import com.ranklands.pagoverbo.model.VotoAutorFrase;
import com.ranklands.pagoverbo.model.VotoAutorTexto;
import com.ranklands.pagoverbo.model.parametro.ParametroBanco;
import com.ranklands.pagoverbo.model.parametro.ParametroExperienciaNivel;
import com.ranklands.pagoverbo.model.parametro.ParametroGameficacao;
import com.ranklands.pagoverbo.model.parametro.ParametroTitulo;
import com.ranklands.pagoverbo.repository.AutorRepository;
import com.ranklands.pagoverbo.repository.parametro.ParametroBancoRepository;
import com.ranklands.pagoverbo.repository.parametro.ParametroExperienciaNivelRepository;
import com.ranklands.pagoverbo.repository.parametro.ParametroGameficacaoRepository;
import com.ranklands.pagoverbo.repository.parametro.ParametroTituloRepository;

import javassist.NotFoundException;

@Service
public class GameficacaoServiceImpl implements GameficacaoService {
	

	@Autowired
	private AutorRepository autorRepository;
	
	@Autowired
	private ParametroBancoRepository parametroBancoRepository;

	@Autowired
	private ParametroExperienciaNivelRepository parametroExperienciaNivelRepository;
	
	@Autowired
	private ParametroGameficacaoRepository parametroGameficacaoRepository;
	
	@Autowired
	private ParametroTituloRepository parametroTituloRepository;

	public static BigDecimal MULTIPLICADOR_PAGO = new BigDecimal("2");

	public void preparaAutorParaGameficacao(Frase frase, Texto texto, Boolean insercaoVoto) throws NotFoundException {
		Optional<Autor> autor = this.autorRepository.findById(frase.getAutor().getId());
		Optional<ParametroBanco> banco = this.parametroBancoRepository.findById(1);
		Optional<ParametroGameficacao> gameficacao = this.parametroGameficacaoRepository.findById(1);
		
		List<ParametroExperienciaNivel> exps = this.parametroExperienciaNivelRepository.findAll();
		List<ParametroTitulo> titulos = this.parametroTituloRepository.findAll();
		
		Boolean fraseTexto = null;
		if(frase != null) {
			fraseTexto = true;
		}
		if(texto!= null) {
			fraseTexto = false;
		}
		
		this.verificacoesObjetos(autor.get(), gameficacao.get(), exps, titulos);
		this.verificacoesBanco(banco.get(), autor.get(), frase, texto, insercaoVoto, fraseTexto);
		this.verificacoesLimitacoesPlano(autor.get(), insercaoVoto, fraseTexto);
		this.insereExp(exps, gameficacao.get(), autor.get(), insercaoVoto, fraseTexto);
		this.verificaTitulo(titulos, gameficacao.get(), autor.get(), insercaoVoto, fraseTexto);
		this.verificaNivel(exps, autor.get());
		
		this.parametroBancoRepository.save(banco.get());
		this.autorRepository.save(autor.get());
	}
	// TODO Verificações de objetos
	@Override
	public void verificacoesObjetos(Autor autor, ParametroGameficacao gameficacao, List<ParametroExperienciaNivel> exps, List<ParametroTitulo> titulos) throws NotFoundException {
		if (autor == null) {
			throw new NotFoundException("Desculpe, não foi possivel encontrar um autor com esse id");
		}
		
		if (gameficacao == null) {
			throw new NotFoundException("Desculpe, não foi possivel encontrar um parâmetro de gamificação");
		}
		
		if(exps.isEmpty()) {
			throw new NotFoundException("Desculpe, não foi possivel encontrar parâmetros de experiências");

		}
		
		if(titulos.isEmpty()) {
			throw new NotFoundException("Desculpe, não foi possivel encontrar parâmetros de titulos");

		}		
	}

	// TODO verificaçõs de banco e inserções de valores na conta
	@Override
	public void verificacoesBanco(ParametroBanco banco, Autor autor, Frase frase, Texto texto, Boolean insercaoVoto, Boolean fraseTexto) {
		BigDecimal saldoBanco = banco.getSaldo();
		if (banco != null && !saldoBanco.equals(BigDecimal.ZERO)) {
			if (insercaoVoto) {
				autor.setContadorFrases(autor.getContadorFrases() + 1);

				if (!autor.getPlano()) {

					if (fraseTexto) {
						autor.setSaldo(autor.getSaldo().add(banco.getTaxaInsercaoFrase()));
						banco.setSaldo(banco.getSaldo().subtract(banco.getTaxaInsercaoFrase()));
					} else if (!fraseTexto) {
						autor.setSaldo(autor.getSaldo().add(banco.getTaxaInsercaoTexto()));
						banco.setSaldo(banco.getSaldo().subtract(banco.getTaxaInsercaoTexto()));
					}

				} else if (autor.getPlano()) {
					if (fraseTexto) {
						autor.setSaldo(autor.getSaldo().add(banco.getTaxaInsercaoFrase().multiply(MULTIPLICADOR_PAGO)));
						banco.setSaldo(
								banco.getSaldo().subtract(banco.getTaxaInsercaoFrase().multiply(MULTIPLICADOR_PAGO)));
					} else if (!fraseTexto) {
						autor.setSaldo(autor.getSaldo().add(banco.getTaxaInsercaoTexto().multiply(MULTIPLICADOR_PAGO)));
						banco.setSaldo(banco.getSaldo().subtract(banco.getTaxaInsercaoTexto().multiply(MULTIPLICADOR_PAGO)));
					}
				}

			} else if (!insercaoVoto) {

				if (fraseTexto) {
					VotoAutorFrase votoAutor = new VotoAutorFrase(null, LocalDate.now(), autor, frase);
					autor.getVotosAutorFrases().add(votoAutor);

					if (!autor.getPlano()) {

						autor.setSaldo(autor.getSaldo().add(banco.getTaxaVotoFrase()));
						banco.setSaldo(banco.getSaldo().subtract(banco.getTaxaVotoFrase()));

					} else if (autor.getPlano()) {

						autor.setSaldo(autor.getSaldo().add(banco.getTaxaVotoFrase().multiply(MULTIPLICADOR_PAGO)));
						banco.setSaldo(banco.getSaldo().subtract(banco.getTaxaVotoFrase().multiply(MULTIPLICADOR_PAGO)));

					}
				}else if (!fraseTexto) {
					VotoAutorTexto votoAutor = new VotoAutorTexto(null, LocalDate.now(), autor, texto);
					autor.getVotosAutorTextos().add(votoAutor);

					if (!autor.getPlano()) {

						autor.setSaldo(autor.getSaldo().add(banco.getTaxaVotoTexto()));
						banco.setSaldo(banco.getSaldo().subtract(banco.getTaxaVotoTexto()));

					} else if (autor.getPlano()) {

						autor.setSaldo(autor.getSaldo().add(banco.getTaxaVotoTexto().multiply(MULTIPLICADOR_PAGO)));
						banco.setSaldo(banco.getSaldo().subtract(banco.getTaxaVotoTexto().multiply(MULTIPLICADOR_PAGO)));

					}					
				}
			}
		}
	}

	// TODO Verificações de limitações de plano
	@Override
	public void verificacoesLimitacoesPlano(Autor autor, Boolean insercaoVoto, Boolean fraseTexto) {
		if(!autor.getPlano()) {

			if(fraseTexto) {
				if(insercaoVoto) {
					if(autor.getContadorFrases() > 10) {
						throw new IllegalArgumentException("Desculpe, mas parece que você chegou no limite máximo de frases, faça um upgrade no plano.");
					}
					
				}else if(!insercaoVoto) {
					
					int contadorVotosDia = 0;
					int contadorVotosMes = 0;
					for(VotoAutorFrase votoFrase : autor.getVotosAutorFrases()) {
						if(votoFrase.getDataLancamento().getDayOfMonth() == LocalDate.now().getDayOfMonth()) {
							contadorVotosDia = contadorVotosDia + 1;
						}
						if(votoFrase.getDataLancamento().getMonthValue() == LocalDate.now().getMonthValue()) {
							contadorVotosMes = contadorVotosMes + 1;
						}
					}
					
					if(contadorVotosDia > 5) {
						throw new IllegalArgumentException("Desculpe, mas parece que você chegou no limite máximo de votações diárias, faça um upgrade no plano.");

					}
					if(contadorVotosMes > 150) {
						throw new IllegalArgumentException("Desculpe, mas parece que você chegou no limite máximo de votações mensais, faça um upgrade no plano.");

					}
				}
				
			}else if(!fraseTexto) {
				if(insercaoVoto) {
					if(autor.getContadorTexto() > 10) {
						throw new IllegalArgumentException("Desculpe, mas parece que você chegou no limite máximo de frases, faça um upgrade no plano.");
					}
					
				}else if(!insercaoVoto) {
					
					int contadorVotosDia = 0;
					int contadorVotosMes = 0;
					for(VotoAutorTexto votoTexto : autor.getVotosAutorTextos()) {
						if(votoTexto.getDataLancamento().getDayOfMonth() == LocalDate.now().getDayOfMonth()) {
							contadorVotosDia = contadorVotosDia + 1;
						}
						if(votoTexto.getDataLancamento().getMonthValue() == LocalDate.now().getMonthValue()) {
							contadorVotosMes = contadorVotosMes + 1;
						}
					}
					
					if(contadorVotosDia > 5) {
						throw new IllegalArgumentException("Desculpe, mas parece que você chegou no limite máximo de votações diárias, faça um upgrade no plano.");

					}
					if(contadorVotosMes > 150) {
						throw new IllegalArgumentException("Desculpe, mas parece que você chegou no limite máximo de votações mensais, faça um upgrade no plano.");

					}
				}				
			}

		}		
	}
	
	// TODO Insere exp na conta
	@Override
	public void insereExp(List<ParametroExperienciaNivel> exps, ParametroGameficacao gameficacao, Autor autor, Boolean insercaoVoto, Boolean fraseTexto) {

		for(ParametroExperienciaNivel paramExperiencia : exps) {
			if(autor.getNivel() == paramExperiencia.getNivel()) {
				BigDecimal resultadoExp = BigDecimal.ZERO;
				BigDecimal valorExpInsercao = BigDecimal.ZERO;
				BigDecimal valorExpVoto = BigDecimal.ZERO;
				BigDecimal multiplicador = BigDecimal.ZERO;
				BigDecimal multiplicadorPlanoPago = BigDecimal.ZERO;
				BigDecimal multiplicadorPlanoFree = BigDecimal.ZERO;

				if(fraseTexto) {
					valorExpInsercao = new BigDecimal(gameficacao.getValorExpFrase());
					valorExpVoto = new BigDecimal(gameficacao.getValorExpVotoFrase());
					multiplicador = paramExperiencia.getMultiplicador();
					multiplicadorPlanoPago = new BigDecimal(gameficacao.getMultiplicadorExpPago());
					multiplicadorPlanoFree = new BigDecimal(gameficacao.getMultiplicadorExpFree());

					if(!autor.getPlano() && insercaoVoto) {
						resultadoExp = valorExpInsercao.multiply(multiplicador).multiply(multiplicadorPlanoFree);								
					
					}else if(autor.getPlano() && insercaoVoto) {
						resultadoExp = valorExpInsercao.multiply(multiplicador).multiply(multiplicadorPlanoPago);								
					}
					
					if(!autor.getPlano() && !insercaoVoto) {
						resultadoExp = valorExpVoto.multiply(multiplicador).multiply(multiplicadorPlanoFree);								
					
					}else if(autor.getPlano() && !insercaoVoto) {
						resultadoExp = valorExpVoto.multiply(multiplicador).multiply(multiplicadorPlanoPago);	
					}
					
					if(resultadoExp != BigDecimal.ZERO) {
						autor.setExp(autor.getExp() + resultadoExp.intValue());		
					}
				}
			}
		}
		
	}
	
	// TODO Verifica se o autor está apto para titulos
	@Override
	public void verificaTitulo(List<ParametroTitulo> titulos, ParametroGameficacao gameficacao, Autor autor, Boolean insercaoVoto, Boolean fraseTexto) {
		Titulo titulo = new Titulo();
		NotificacaoAutor notificacao = new NotificacaoAutor();
		for(ParametroTitulo parametroTitulo : titulos) {
						
			if(fraseTexto) {
				if(insercaoVoto && !parametroTitulo.getTipo_voto()) {
					if( autor.getContadorFrases() == parametroTitulo.getQuantidade() && parametroTitulo.getTipoFraseTexto() == true) {
						titulo.setId(null);
						titulo.setNome(parametroTitulo.getNome());
						titulo.setDescricao(parametroTitulo.getDescricao());
						titulo.setAutor(autor);
						
						if(!autor.getPlano()) {
							autor.setExp(autor.getExp() + (gameficacao.getValorExpFrase() * gameficacao.getMultiplicadorExpFree() ));

						}else if(autor.getPlano()) {
							autor.setExp(autor.getExp() + (gameficacao.getValorExpFrase() * gameficacao.getMultiplicadorExpPago() ));
						}
					}
					
				}else if(!insercaoVoto && parametroTitulo.getTipo_voto()) {
					if( autor.getVotosAutorFrases().size() == parametroTitulo.getQuantidade() && parametroTitulo.getTipoFraseTexto() == true) {
						titulo.setId(null);
						titulo.setNome(parametroTitulo.getNome());
						titulo.setDescricao(parametroTitulo.getDescricao());
						titulo.setAutor(autor);
						
						if(!autor.getPlano()) {
							autor.setExp(autor.getExp() + (gameficacao.getValorExpVotoFrase()* gameficacao.getMultiplicadorExpFree() ));

						}else if(autor.getPlano()) {
							autor.setExp(autor.getExp() + (gameficacao.getValorExpVotoFrase() * gameficacao.getMultiplicadorExpPago() ));
						}
					}
					
				}				
			}else if (!fraseTexto) {
				if(insercaoVoto && !parametroTitulo.getTipo_voto()) {
					if( autor.getContadorTexto() == parametroTitulo.getQuantidade() && parametroTitulo.getTipoFraseTexto() == false) {
						titulo.setId(null);
						titulo.setNome(parametroTitulo.getNome());
						titulo.setDescricao(parametroTitulo.getDescricao());
						titulo.setAutor(autor);
						
						if(!autor.getPlano()) {
							autor.setExp(autor.getExp() + (gameficacao.getValorExpTexto() * gameficacao.getMultiplicadorExpFree() ));

						}else if(autor.getPlano()) {
							autor.setExp(autor.getExp() + (gameficacao.getValorExpTexto() * gameficacao.getMultiplicadorExpPago() ));
						}
					}
					
				}else if(!insercaoVoto && parametroTitulo.getTipo_voto()) {
					if( autor.getVotosAutorTextos().size() == parametroTitulo.getQuantidade() && parametroTitulo.getTipoFraseTexto() == false) {
						titulo.setId(null);
						titulo.setNome(parametroTitulo.getNome());
						titulo.setDescricao(parametroTitulo.getDescricao());
						titulo.setAutor(autor);
						
						if(!autor.getPlano()) {
							autor.setExp(autor.getExp() + (gameficacao.getValorExpTexto()* gameficacao.getMultiplicadorExpFree() ));

						}else if(autor.getPlano()) {
							autor.setExp(autor.getExp() + (gameficacao.getValorExpTexto() * gameficacao.getMultiplicadorExpPago() ));
						}
					}
					
				}				
			}
		}
	
		if(titulo.getAutor() != null) {
			autor.getTitulos().add(titulo);
			
			notificacao.setAutor(autor);
			notificacao.setTitulo("Sistema");
			notificacao.setTexto("Parabéns " + autor.getNome() + ", você adquiriu o titulo" + " '" + titulo.getNome()+ "'.");
			notificacao.setData(LocalDateTime.now());
			notificacao.setMesReferencia(LocalDateTime.now().getMonthValue());

			autor.getNotificacoes().add(notificacao);
		}

	}

	// TODO Verifica se o autor está apto para algum outro nível
	@Override
	public void verificaNivel(List<ParametroExperienciaNivel> exps, Autor autor) {
		NotificacaoAutor notificacao = new NotificacaoAutor();
		
		for(ParametroExperienciaNivel exp : exps) {
			if(autor.getExp() >= exp.getExperiencia() && autor.getNivel() < exp.getNivel()) {
				autor.setNivel(exp.getNivel());
				
				notificacao.setAutor(autor);
				notificacao.setTitulo("Sistema");
				notificacao.setTexto("Parabéns " + autor.getNome() + ", você passou para o nível " + autor.getNivel());
				notificacao.setData(LocalDateTime.now());
				notificacao.setMesReferencia(LocalDateTime.now().getMonthValue());
				
				autor.getNotificacoes().add(notificacao);
			}			
		}	
	}

}
