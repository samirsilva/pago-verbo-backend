package com.ranklands.pagoverbo.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.ranklands.pagoverbo.model.Autor;
import com.ranklands.pagoverbo.model.Frase;
import com.ranklands.pagoverbo.model.VotoAutorFrase;
import com.ranklands.pagoverbo.repository.AutorRepository;
import com.ranklands.pagoverbo.repository.FraseRepository;
import com.ranklands.pagoverbo.request.FraseRequest;
import com.ranklands.pagoverbo.request.VotoFraseRequest;
import com.ranklands.pagoverbo.service.exception.DataIntegrityException;
import com.ranklands.pagoverbo.service.exception.ObjectNotFoundException;
import com.ranklands.pagoverbo.service.gameficacao.GameficacaoService;

import javassist.NotFoundException;

@Service
public class FraseService {

	@Autowired
	private FraseRepository fraseRepository;

	@Autowired
	private AutorRepository autorRepository;
	
	@Autowired
	private GameficacaoService gameficacao;

	public static BigDecimal MULTIPLICADOR_PAGO = new BigDecimal("2");

	public Autor findAutor(Integer id) {
		Optional<Autor> autor = this.autorRepository.findById(id);
		return autor.orElseThrow(() -> new ObjectNotFoundException("Objeto não encontrado! Id: " + id + ", Tipo: " + Autor.class.getName()));
	}
	
	public Frase find(Integer id) {
		Optional<Frase> frase = this.fraseRepository.findById(id);
		return frase.orElseThrow(() -> new ObjectNotFoundException("Objeto não encontrado! Id: " + id + ", Tipo: " + Frase.class.getName()));
	}

	public Frase insert(Frase frase) throws NotFoundException{
		
		this.gameficacao.preparaAutorParaGameficacao(frase,null, true);
		
		frase.setId(null);
		return this.fraseRepository.save(frase);
	}

	public Frase update(FraseRequest request, Integer id) throws NotFoundException {
		Frase newFrase = this.find(id);
		this.findAutor(request.getAutorId());

		Boolean votos = false;

		if(request.getTexto() != null) {
			newFrase.setTexto(request.getTexto());
		}
		if(votos) {
			this.gameficacao.preparaAutorParaGameficacao(newFrase,null, false);
		}

		return this.fraseRepository.save(newFrase);
	}
	
	public Frase updateVoto(VotoFraseRequest requestVoto, Integer id) throws NotFoundException {
		Frase newFrase = this.find(id);
		Autor autor = this.findAutor(requestVoto.getAutorId());
		VotoAutorFrase votoAutor = null;

		Boolean votos = false;

		if(request.getTexto() != null) {
			newFrase.setTexto(request.getTexto());
		}
		if(votos) {
			this.gameficacao.preparaAutorParaGameficacao(newFrase,null, false);
		}

		return this.fraseRepository.save(newFrase);
	}
	
	public void delete(Integer id) {
		find(id);
		
		try {
			this.fraseRepository.deleteById(id);
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possivel excluir a frase");
		}
	}

	public List<Frase> findAll() {
		return this.fraseRepository.findAllByOrderByIdDesc();
	}

	public Page<Frase> findPage(Integer page, Integer linesPage, String orderBy, String direction) {
		
		PageRequest pageRequest = PageRequest.of(page, linesPage, Direction.valueOf(direction), orderBy);
		return this.fraseRepository.findAll(pageRequest);
	}

	public Frase fromDTO(FraseRequest request) throws NotFoundException {
		Autor autor = this.findAutor(request.getAutorId());
		String nomeAutor = autor.getNome() + " " + autor.getSobrenome();

		return new Frase(null, request.getTexto(), LocalDate.now(), 0, 
				0, autor, LocalDate.now().getMonthValue(), nomeAutor);
	}
}