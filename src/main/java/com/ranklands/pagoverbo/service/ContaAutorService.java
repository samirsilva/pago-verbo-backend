package com.ranklands.pagoverbo.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.ranklands.pagoverbo.model.Autor;
import com.ranklands.pagoverbo.model.ContaAutor;
import com.ranklands.pagoverbo.repository.AutorRepository;
import com.ranklands.pagoverbo.repository.ContaAutorRepository;
import com.ranklands.pagoverbo.request.ContaAutorRequest;
import com.ranklands.pagoverbo.service.exception.DataIntegrityException;
import com.ranklands.pagoverbo.service.exception.ObjectNotFoundException;

import javassist.NotFoundException;

@Service
public class ContaAutorService {

	@Autowired
	private ContaAutorRepository contaAutorRepository;

	@Autowired
	private AutorRepository autorRepository;

	public ContaAutor find(Integer id) {
		Optional<ContaAutor> obj = this.contaAutorRepository.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException("Objeto não encontrado! Id: " + id + ", Tipo: " + ContaAutor.class.getName()));
	}

	public ContaAutor insert(ContaAutor obj) {
		obj.setId(null);
		return this.contaAutorRepository.save(obj);
	}

	public ContaAutor update(ContaAutor obj) {
		ContaAutor newObj = find(obj.getId());
		return this.contaAutorRepository.save(newObj);
	}

	public void delete(Integer id) {
		find(id);
		
		try {
			this.contaAutorRepository.deleteById(id);
		} catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possivel excluir a conta");
		}
	}

	public Page<ContaAutor> findPage(Integer page, Integer linesPage, String orderBy, String direction) {
		
		PageRequest pageRequest = PageRequest.of(page, linesPage, Direction.valueOf(direction), orderBy);
		return this.contaAutorRepository.findAll(pageRequest);
	}

	public ContaAutor fromDTO(ContaAutorRequest request) throws NotFoundException {

		Optional<Autor> autor = this.autorRepository.findById(request.getAutorId());
		if (autor.get() == null) {
			throw new NotFoundException("Desculpe, não foi possivel encontrar um auto com esse id"); 
		}

		return new ContaAutor(null, request.getNomeCompletoTitular(), request.getBanco(),
				request.getAgencia(), request.getConta(), request.getTipoConta(),
				request.getCpfCnpj(), request.getDigitoConta(), autor.get());
	}
}