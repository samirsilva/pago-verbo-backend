package com.ranklands.pagoverbo.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ranklands.pagoverbo.model.Autor;
import com.ranklands.pagoverbo.model.NotificacaoAutor;
import com.ranklands.pagoverbo.model.SolicitacaoSaque;
import com.ranklands.pagoverbo.model.enums.Status;
import com.ranklands.pagoverbo.model.enums.TipoUsuario;
import com.ranklands.pagoverbo.repository.AutorRepository;
import com.ranklands.pagoverbo.repository.SolicitacaoSaqueRepository;
import com.ranklands.pagoverbo.request.AlterarSaqueRequest;
import com.ranklands.pagoverbo.request.SolicitacaoSaqueRequest;
import com.ranklands.pagoverbo.security.UserSS;
import com.ranklands.pagoverbo.service.exception.AuthorizationException;
import com.ranklands.pagoverbo.service.exception.ObjectNotFoundException;

@Service
public class SaqueService {

	@Autowired
	private AutorRepository autorRepository;

	/*
	 * @Autowired private NotificacaoAutorRepository notificacaoAutorRepository;
	 * 
	 */
	  
	@Autowired 
	private SolicitacaoSaqueRepository solicitacaoSaqueRepository;
	 
	
	public Autor find(Integer id) {

		UserSS user = UserService.authenticated();
		if (user == null || !user.hasHole(TipoUsuario.USUARIO) && !id.equals(user.getId())) {
			throw new AuthorizationException("Acesso negado");
		}

		Optional<Autor> obj = this.autorRepository.findById(id);

		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Usuário não encontrado! Id: " + id + ", Tipo: " + Autor.class.getName()));
	}

	public SolicitacaoSaque insert(SolicitacaoSaqueRequest request, Integer id) {
		Autor autor = find(id);
		SolicitacaoSaque saque = new SolicitacaoSaque();
		NotificacaoAutor notificacao = new NotificacaoAutor();
		
		BigDecimal numeroCem = new BigDecimal("100");
		LocalDate dataInsercao = LocalDate.now();
		
		if(request != null) {
			if (autor.getPlano()) {
				if (autor.getSaldo().floatValue() >= numeroCem.floatValue()) {
					saque.setDataSaque(dataInsercao);
					saque.setValorSaque(request.getValorSaque());
					saque.setStatus(Status.PENDENTE);
					saque.setDescricao("Saque solicitado para o autor " + autor.getNome() + " " + autor.getSobrenome()
							+ " em " + dataInsercao + ". Conta: " + autor.getContaAutor().getConta() + " Agência: "
							+ autor.getContaAutor().getAgencia() + "Banco: " + autor.getContaAutor().getBanco());
					saque.setAutor(autor);
				}else {
					
					throw new IllegalArgumentException(autor.getNome() + ", seu saldo não é suficiente.");
				}

			} else {
				if (autor.getSaldo().floatValue() >= numeroCem.multiply(new BigDecimal("2")).floatValue()) {
					saque.setDataSaque(dataInsercao);
					saque.setValorSaque(request.getValorSaque());
					saque.setStatus(Status.PENDENTE);
					saque.setDescricao("Saque solicitado para o autor " + autor.getNome() + " " + autor.getSobrenome()
							+ " em " + dataInsercao + ". Conta: " + autor.getContaAutor().getConta() + " Agência: "
							+ autor.getContaAutor().getAgencia() + "Banco: " + autor.getContaAutor().getBanco());
					saque.setAutor(autor);
					saque.setNotificacao(false);
				}else {
					
					throw new IllegalArgumentException(autor.getNome() + ", seu saldo não é suficiente.");
				}
			}

			if (saque != null) {
				autor.setSaldo(autor.getSaldo().subtract(saque.getValorSaque()));

				notificacao.setTitulo("Financeiro");
				notificacao.setTexto(autor.getNome() +", a solicitação de saque de valor " + saque.getValorSaque() +
						" está em processamento, em até 72 horas úteis após a validação dos dados bancários o valor será depositado na conta cadastrada. Fique de olho.");
				notificacao.setData(LocalDateTime.now());
				notificacao.setMesReferencia(LocalDateTime.now().getMonthValue());
				notificacao.setAutor(autor);
				
				autor.getNotificacoes().add(notificacao);
				autor.getSaques().add(saque);
			
				this.autorRepository.save(autor);
			}
						
		}else {
			
			throw new IllegalArgumentException(autor.getNome() + ", parece que você não informou o valor do saque");
		}
		
		return saque;
	}

	public SolicitacaoSaque update(AlterarSaqueRequest request, Integer id) {
		Optional<SolicitacaoSaque> saque = this.solicitacaoSaqueRepository.findById(id);
		Autor autor = find(saque.get().getAutor().getId());
		
		saque.get().setStatus(request.getStatus());
		
		if(request.getMotivoAlteracaoStatus() != null && request.getStatus() == Status.RECUSADO) {
			saque.get().setMotivoAlteracaoStatus(request.getMotivoAlteracaoStatus());
		}
		
		if(request.getStatus() == Status.RECUSADO) {
			autor.setSaldo(autor.getSaldo().add(saque.get().getValorSaque()));
		}
		
		this.autorRepository.save(autor);
		this.solicitacaoSaqueRepository.save(saque.get());
		
		return saque.get();
	}

	public List<SolicitacaoSaque> findAll() {
		
		List<SolicitacaoSaque> saques = this.solicitacaoSaqueRepository.findAllByOrderByDataSaqueDesc();
		
		return saques;
	}
	
}