package com.ranklands.pagoverbo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ranklands.pagoverbo.model.Autor;
import com.ranklands.pagoverbo.model.NotificacaoAutor;
import com.ranklands.pagoverbo.model.enums.TipoUsuario;
import com.ranklands.pagoverbo.repository.AutorRepository;
import com.ranklands.pagoverbo.repository.NotificacaoAutorRepository;
import com.ranklands.pagoverbo.security.UserSS;
import com.ranklands.pagoverbo.service.exception.AuthorizationException;
import com.ranklands.pagoverbo.service.exception.ObjectNotFoundException;

@Service
public class NotificacaoAutorService {

	@Autowired
	private AutorRepository autorRepository;

	@Autowired
	private NotificacaoAutorRepository notificacaoAutorRepository;
	
	public Autor find(Integer id) {

		UserSS user = UserService.authenticated();
		if (user == null || !user.hasHole(TipoUsuario.USUARIO) && !id.equals(user.getId())) {
			throw new AuthorizationException("Acesso negado");
		}

		Optional<Autor> obj = this.autorRepository.findById(id);

		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Usuário não encontrado! Id: " + id + ", Tipo: " + Autor.class.getName()));
	}

	public List<NotificacaoAutor> findAll(Integer id) {
		
		return this.notificacaoAutorRepository.findByAutorIdOrderByDataDesc(id);
	}
	
}
