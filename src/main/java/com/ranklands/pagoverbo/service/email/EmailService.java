package com.ranklands.pagoverbo.service.email;

import javax.mail.internet.MimeMessage;

import org.springframework.mail.SimpleMailMessage;

import com.ranklands.pagoverbo.model.Autor;

public interface EmailService {

	void sendOrderConfirmationEmail(Autor obj);
	
	void sendEmail(SimpleMailMessage msg);
	
	void sendOrderConfirmationHtmlEmail(Autor obj);
	
	void sendHtmlEmail(MimeMessage msg);
	
	void sendNewPasswordEmail(Autor autor, String newPass);

}
