package com.ranklands.pagoverbo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ranklands.pagoverbo.model.VotoAutorTexto;

@Repository
public interface VotoTextoRepository extends JpaRepository<VotoAutorTexto, Integer> {

}
