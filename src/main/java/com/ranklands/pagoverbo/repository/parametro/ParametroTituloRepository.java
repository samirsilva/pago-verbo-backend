package com.ranklands.pagoverbo.repository.parametro;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ranklands.pagoverbo.model.parametro.ParametroTitulo;

@Repository
public interface ParametroTituloRepository extends JpaRepository<ParametroTitulo, Integer> {

}
