package com.ranklands.pagoverbo.repository.parametro;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ranklands.pagoverbo.model.parametro.ParametroBanco;

@Repository
public interface ParametroBancoRepository extends JpaRepository<ParametroBanco, Integer> {

}
