package com.ranklands.pagoverbo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ranklands.pagoverbo.model.SolicitacaoSaque;

@Repository
public interface SolicitacaoSaqueRepository extends JpaRepository<SolicitacaoSaque, Integer> {

	public List<SolicitacaoSaque> findAllByOrderByDataSaqueDesc();
}
