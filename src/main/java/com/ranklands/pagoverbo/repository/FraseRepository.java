package com.ranklands.pagoverbo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ranklands.pagoverbo.model.Frase;

@Repository
public interface FraseRepository extends JpaRepository<Frase, Integer> {

	public List<Frase> findAllByOrderByIdDesc();
}
