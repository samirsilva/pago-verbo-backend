package com.ranklands.pagoverbo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ranklands.pagoverbo.model.Texto;

@Repository
public interface TextoRepository extends JpaRepository<Texto, Integer> {

	public List<Texto> findAllByOrderByIdDesc();
}
