package com.ranklands.pagoverbo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ranklands.pagoverbo.model.ExtornoValorBanco;

@Repository
public interface ExtornoValorBancoRepository extends JpaRepository<ExtornoValorBanco, Integer> {

}
