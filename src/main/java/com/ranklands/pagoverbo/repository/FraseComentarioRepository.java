package com.ranklands.pagoverbo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ranklands.pagoverbo.model.FraseComentario;

@Repository
public interface FraseComentarioRepository extends JpaRepository<FraseComentario, Integer> {

	public List<FraseComentario> findAllByOrderByIdDesc();
}
