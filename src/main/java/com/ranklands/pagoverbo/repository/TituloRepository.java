package com.ranklands.pagoverbo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ranklands.pagoverbo.model.Titulo;

@Repository
public interface TituloRepository extends JpaRepository<Titulo, Integer> {

	public List<Titulo> findAllByOrderByIdDesc();
}
