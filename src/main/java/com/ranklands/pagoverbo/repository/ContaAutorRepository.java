package com.ranklands.pagoverbo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ranklands.pagoverbo.model.ContaAutor;

@Repository
public interface ContaAutorRepository extends JpaRepository<ContaAutor, Integer> {

}
