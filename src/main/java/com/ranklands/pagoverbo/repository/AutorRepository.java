package com.ranklands.pagoverbo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ranklands.pagoverbo.model.Autor;

@Repository
public interface AutorRepository extends JpaRepository<Autor, Integer> {

	@Transactional(readOnly = true)
	public Autor findByEmail (String email);
	
	@Transactional(readOnly = true)
	public Autor findByCpf (String cpf);
	
	@Transactional(readOnly = true)
	public List<Autor> findAllByOrderByVotoPositivoDesc();
}
