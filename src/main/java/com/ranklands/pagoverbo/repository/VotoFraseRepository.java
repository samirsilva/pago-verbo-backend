package com.ranklands.pagoverbo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ranklands.pagoverbo.model.VotoAutorFrase;

@Repository
public interface VotoFraseRepository extends JpaRepository<VotoAutorFrase, Integer> {

}
