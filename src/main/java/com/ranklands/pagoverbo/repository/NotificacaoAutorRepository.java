package com.ranklands.pagoverbo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ranklands.pagoverbo.model.NotificacaoAutor;

@Repository
public interface NotificacaoAutorRepository extends JpaRepository<NotificacaoAutor, Integer> {

	public List<NotificacaoAutor> findByAutorIdOrderByDataDesc(Integer id);
}
